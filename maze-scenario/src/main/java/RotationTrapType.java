/**
 * Represents one of the two "rotation traps", or the absence thereof.
 */
public enum RotationTrapType
{
    NONE(null, 0),
    CLOCKWISE("mono-rotation-cw.png", -1),
    ANTICLOCKWISE("mono-rotation-acw.png", 1);
    
    private final String imageName;
    private final int delta;

    private RotationTrapType(String imageName, int delta)
    {
        this.imageName = imageName;
        this.delta = delta;
    }

    /**
     * Returns the filename of the image to be used for this rotation trap, or null for no
     * image.
     */
    public String getImageName()
    {
        return imageName;
    }
   
    /** 
     * Returns the number of 90-degree increments to be added to the maze angle as a result of this
     * rotation type.
     */
    public int getDelta()
    {
        return delta;
    }
}
