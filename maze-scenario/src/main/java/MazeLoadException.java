import edu.curtin.cs.codedemo.scenario.ScenarioSetupException; 

public class MazeLoadException extends ScenarioSetupException
{
    private String filename = "[unspecified]";

    public MazeLoadException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
    
    public MazeLoadException(String msg)
    {
        super(msg);
    }
    
    public MazeLoadException forFile(String filename)
    {
        this.filename = filename;
        return this;
    }
    
    @Override
    public String getMessage()
    {
        return String.format("Could not load maze file '%s': %s", filename, super.getMessage());
    }
}
