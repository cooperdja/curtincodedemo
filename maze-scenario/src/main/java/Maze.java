import edu.curtin.cs.codedemo.scenario.display.IntPoint;

import java.awt.Color;
import java.util.*;

/** 
 * <p>Represents a maze, consisting of a set of horizontal and vertical walls, rotation traps, 
 * starting locations of the robot and trophy, initial angle of the maze grid, and colour of the
 * maze.</p>
 */
public class Maze
{
    private Color wallColour = Color.BLUE;
    private Color bgColour = Color.WHITE;
    
    private boolean[][] horizWalls;
    private List<IntPoint> horizWallList = new ArrayList<>();
    
    private boolean[][] vertWalls;
    private List<IntPoint> vertWallList = new ArrayList<>();
    
    private Map<IntPoint,RotationTrap> rotationTraps = new HashMap<>();
    
    private int initialRotation = 0;
    private IntPoint initialRobotPosition = IntPoint.ORIGIN;
    private IntPoint trophyPosition = IntPoint.ORIGIN;
    private IntPoint size;
        
    public Maze(int nRows, int nCols)
    {
        this.size = new IntPoint(nRows, nCols);
        horizWalls = new boolean[nRows + 1][nCols];
        vertWalls = new boolean[nRows][nCols + 1];
    }
    
    public void setWallColour(Color c)
    {
        this.wallColour = c;
    }
    
    public void setBgColour(Color c)
    {
        this.bgColour = c;
    }
    
    public void setHorizWall(IntPoint pos, boolean present)
    {
        horizWalls[pos.getRow()][pos.getCol()] = present;
        if(present)
        {
            horizWallList.add(pos);
        }
    }
    
    public void setVertWall(IntPoint pos, boolean present)
    {
        vertWalls[pos.getRow()][pos.getCol()] = present;
        if(present)
        {
            vertWallList.add(pos);
        }
    }
    
    public void setInitialRobotPosition(IntPoint pos)
    {
        initialRobotPosition = pos;
    }
    
    public void setTrophyPosition(IntPoint pos)
    {
        trophyPosition = pos;
    }
    
    public void addRotationTrap(IntPoint pos, RotationTrapType type)
    {
        rotationTraps.put(pos, new RotationTrap(type, pos));
    }
    
    public int getInitialRotation()
    {
        return initialRotation;
    }
    
    public Color getWallColour()
    {
        return wallColour;
    }
    
    public Color getBgColour()
    {
        return bgColour;
    }
    
    public IntPoint getInitialRobotPosition()
    {
        return initialRobotPosition;
    }
    
    public IntPoint getTrophyPosition()
    {
        return trophyPosition;
    }
    
    /**
     * Returns the overall size of the maze as an IntPoint object. (This is effectively the 
     * position immediately outside the bottom-right corner of the maze, to the bottom right.)
     */      
    public IntPoint getSize()
    {
        return size;
    }

    /** 
     * Returns true if there is a horizontal wall immediately above the given position.
     */
    public boolean horizWall(IntPoint pos)
    {
        return pos.index(horizWalls);
    }
    
    /**
     * Returns true if there is a vertical wall immediately to the left of the given position.
     */
    public boolean vertWall(IntPoint pos)
    {
        return pos.index(vertWalls);
    }
    
    /**
     * Returns a RotationTrapType representing the type of rotation trap at a given position.
     * This will be be RotationTrapType.NONE if there is no trap there.
     */
    public RotationTrapType rotationTrap(IntPoint pos)
    {
        RotationTrap trap = rotationTraps.get(pos);
        if(trap == null)
        {
            return RotationTrapType.NONE;
        }
        else
        {
            return trap.getType();
        }
    }
    
    /**
     * Retrieve a list of horizontal wall segments, each identified by the row/column position that
     * it is immediately above. 
     */
    public List<IntPoint> getHorizWalls()
    {
        return Collections.unmodifiableList(horizWallList);
    }
    
    /**
     * Retrieve a list of vertical wall segments, each identified by the row/column position that 
     * it is immediately to the left of.
     */
    public List<IntPoint> getVertWalls()
    {
        return Collections.unmodifiableList(vertWallList);
    }
    
    /**
     * Retrieve a list of rotation traps, where RotationTrap consists of the type and position of
     * each trap. (This <em>does not</em> return entries with RotationTrapType.NONE.)
     */
    public Collection<RotationTrap> getRotationTraps()
    {
        return Collections.unmodifiableCollection(rotationTraps.values());
    }
}

