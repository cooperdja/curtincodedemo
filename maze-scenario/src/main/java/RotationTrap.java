import edu.curtin.cs.codedemo.scenario.display.IntPoint;

/**
 * Represents a particular rotation trap within the maze. This consists of a type and a position.
 */
public class RotationTrap
{
    private final RotationTrapType type;
    private final IntPoint position;

    public RotationTrap(RotationTrapType type, IntPoint position)
    {
        this.type = type;
        this.position = position;
    }
    
    /** Get the type of rotation trap. */
    public RotationTrapType getType()
    {
        return type;
    }
    
    /** Get the trap's row/column position within the maze. */
    public IntPoint getPosition()
    {   
        return position;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        boolean eq = false;
        if(obj instanceof RotationTrap)
        {
            RotationTrap r = (RotationTrap)obj;
            eq = (type == r.type) && position.equals(r.position);
        }
        return eq;
    }
    
    @Override
    public int hashCode()
    {
        return type.hashCode() * 37 + position.hashCode();
    }
}
