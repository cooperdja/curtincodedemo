import edu.curtin.cs.codedemo.scenario.*;
import edu.curtin.cs.codedemo.scenario.display.IntPoint;

import java.awt.Color;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.*;

public class MazeReader
{
    private ScenarioResources resources;

    public MazeReader(ScenarioResources resources)
    {
        this.resources = resources;
    }
    
    private static Color parseColour(String colourStr) throws MazeLoadException
    {
        try
        {
            Field colourField = Color.class.getField(colourStr);
            return (Color)colourField.get(null);
        }
        catch(ClassCastException | ReflectiveOperationException e) {}
        
        try
        {
            return Color.decode(colourStr);
        }
        catch(NumberFormatException e)
        {
            throw new MazeLoadException(String.format("colour '%s' not valid", colourStr), e);
        }
    }
    
    private static abstract class Config 
    {
        String key;
        boolean specified;
        boolean required;
        
        Config(String key, boolean required)
        {
            this.key = key;
            this.required = required;
        }
        
        abstract void accept(String value) throws MazeLoadException;
        
        void validate() throws MazeLoadException
        {
            if(!specified && required)
            {
                throw new MazeLoadException(String.format("%s not specified", key));
            }
        }
        
        static Map<String,Config> map(Config... configs)
        {
            Map<String,Config> configMap = new HashMap<>();
            for(Config cfg : configs)
            {
                configMap.put(cfg.key, cfg);
            }
            return configMap;
        }
    }
    
    private static class SizeConfig extends Config
    {
        int nRows = -1;
        int nCols = -1;
        
        SizeConfig()
        {
            super("size", true);
        }
        
        @Override
        void accept(String value) throws MazeLoadException
        {
            try
            {
                Scanner sc = new Scanner(value).useDelimiter("\\s*,?\\s*");
                nRows = sc.nextInt();
                nCols = sc.nextInt();
                specified = true;
            }
            catch(NoSuchElementException e)
            {
                throw new MazeLoadException(String.format("size '%s' not valid", value), e);
            }
        }
    }
    
    private static class ColourConfig extends Config
    {
        Color colour = Color.BLUE;
        
        ColourConfig(String what)
        {
            super(what + "-colour", false);
        }
        
        @Override
        void accept(String value) throws MazeLoadException
        {
            colour = parseColour(value);
        }
    }
        
    private static final Pattern HEADER_PATTERN = 
        Pattern.compile("\\s*([^\\s=]*)\\s*=\\s*(|.*[^\\s])\\s*");
        
    public Maze readMaze(String filename) throws MazeLoadException
    {
        try
        {
            SizeConfig size = new SizeConfig();
            ColourConfig wallColour = new ColourConfig("wall");
            ColourConfig bgColour = new ColourConfig("background");
            Map<String,Config> configs = Config.map(size, wallColour, bgColour);
            
            String fileText;
            try
            {   
                fileText = resources.getTextFile(filename);
            }
            catch(IOException e)
            {
                throw new MazeLoadException("IO error: " + e.getMessage(), e);
            }

            // Put all lines into list.
            List<String> lines = new LinkedList<>(Arrays.asList(
                fileText
                    .replaceAll("//[^\n]*", "")     // Erase C++-style comments
                    .replaceAll("([ \t]\n)+", "\n") // Erase blank lines and end-of-line spaces
                    .split("\n")));
                
            Matcher matcher = HEADER_PATTERN.matcher(lines.get(0));
            while(matcher.matches())
            {
                Config cfg = configs.get(matcher.group(1).toLowerCase());
                if(cfg == null)
                {
                    throw new MazeLoadException(String.format(
                        "'%s' is not a recognised config option", 
                        matcher.group(1)));
                }
                cfg.accept(matcher.group(2));
                
                lines.remove(0);
                matcher = HEADER_PATTERN.matcher(lines.get(0));
            }
            
            for(Config cfg : configs.values())
            {
                cfg.validate();
            }
            
            Maze maze = new Maze(size.nRows, size.nCols);
            maze.setWallColour(wallColour.colour);
            maze.setBgColour(bgColour.colour);
            
            // Validate number of rows
            if(lines.size() != size.nRows + 1)
            {
                throw new MazeLoadException(String.format(
                    "expected %d lines following header, but found %d",
                    size.nRows + 1, lines.size()));
            }
            
            // Validate number of columns. (It's okay if it's shorter than the required length, 
            // because we'll just pad it with spaces.)
            int maxLineLength = size.nCols * 3 + 1;
            for(String line : lines)
            {
                if(line.length() > maxLineLength)
                {
                    throw new MazeLoadException(String.format(
                        "expected line to be at most %d characters long, but found %d",
                        maxLineLength, line.length()));
                }
            }
            
            String nextLine = lines.remove(0);
            char[] lineChars = new char[size.nCols * 3 + 1];
            
            Arrays.fill(lineChars, ' ');
            nextLine.getChars(0, nextLine.length(), lineChars, 0);
            
            char vertWallChar, horizWallChar, objChar;
            
            // The first line represents whether there are horizontal walls along the top of the 
            // maze. Generally this should be true, although the option is there to leave them out.
            //
            // To align with subsequent rows (that include other information), the first line 
            // consists of triplets of characters that are either " _ " (space-underscore-space)
            // or "   " (three spaces). The underscore version represents a horizontal wall.
            for(int j = 0; j < size.nCols; j++)
            {
                int c = j * 3;
                vertWallChar = lineChars[c];
                horizWallChar = lineChars[c + 1];
                objChar = lineChars[c + 2];
                
                if(vertWallChar != ' ' || objChar != ' ' || "_ ".indexOf(horizWallChar) == -1)
                {
                    throw new MazeLoadException(String.format(
                        "Invalid or out-of-place character at row -1, col %d)", j));
                }
                
                maze.setHorizWall(new IntPoint(0, j), horizWallChar == '_');
            }
            
            // Subsequent rows start with a '|' or ' ' indicating the presence or absence of the
            // left-most wall along the maze edge.
            //
            // The rest of each row consists of triplets of characters:
            // (1) '_' or ' ', indicating the presence or absence of a horizontal wall below the 
            //     corresponding cell.
            // (2) 'R', 'T', 'C', 'A' or ' ', indicating that the maze cell contains the robot 
            //     (intitially), the trophy, a clockwise rotation trap, an anticlockwise trap, or
            //     is empty.
            // (3) '|' or ' ', indicating the presence or absence of a vertical wall right of the
            //     cell.
            for(int i = 0; i < size.nRows; i++)
            {
                nextLine = lines.remove(0);
                Arrays.fill(lineChars, ' ');
                nextLine.getChars(0, nextLine.length(), lineChars, 0);
                
                vertWallChar = lineChars[0];
                if("| ".indexOf(vertWallChar) == -1)
                {
                    throw new MazeLoadException(String.format(
                        "Invalid or out-of-place character at row %d, col -1)", i));
                }
                
                maze.setVertWall(new IntPoint(i, 0), vertWallChar == '|');
                
                for(int j = 0; j < size.nCols; j++)
                {
                    int c = j * 3 + 1;
                    IntPoint pos = new IntPoint(i, j);
                    
                    horizWallChar = lineChars[c];
                    objChar = lineChars[c + 1];
                    vertWallChar = lineChars[c + 2];
                    
                    if( "| ".indexOf(vertWallChar) == -1 ||
                        "_ ".indexOf(horizWallChar) == -1 ||
                        "RTCA ".indexOf(objChar) == -1)
                    {
                        throw new MazeLoadException(String.format(
                            "Invalid or out-of-place character at row %d, col %d)", i, j));
                    }
                    
                    maze.setVertWall(pos.right(), vertWallChar == '|');
                    maze.setHorizWall(pos.down(), horizWallChar == '_');
                    
                    switch(objChar)
                    {
                        case 'R':
                            maze.setInitialRobotPosition(pos);
                            break;
                            
                        case 'T':
                            maze.setTrophyPosition(pos);
                            break;
                            
                        case 'C':
                            maze.addRotationTrap(pos, RotationTrapType.CLOCKWISE);
                            break;
                            
                        case 'A':
                            maze.addRotationTrap(pos, RotationTrapType.ANTICLOCKWISE);
                            break;
                            
                        case ' ':
                            break;
                            
                        default:
                            throw new AssertionError();
                    }                    
                }
            }
                
            return maze;
        }
        catch(MazeLoadException e)
        {
            throw e.forFile(filename);
        }
    }
}
