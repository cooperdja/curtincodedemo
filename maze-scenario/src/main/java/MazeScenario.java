import edu.curtin.cs.codedemo.scenario.*;
import edu.curtin.cs.codedemo.scenario.display.*;
import edu.curtin.cs.codedemo.scenario.input.*;
import edu.curtin.cs.codedemo.ui.*;

import java.awt.Color;
import java.util.*;
import java.io.IOException;

/** 
 * The entry-point and main class for the maze scenario.
 */
@ScenarioDetails(name     = "Maze",
                 author   = "David Cooper",
                 iconFile = "Blue-Robot.png",
                 helpFile = "maze.html")
         
public class MazeScenario extends Scenario
{
    private static final double BUDGE_DISTANCE = 0.1;
    private static final double ANIMATION_FRAMERATE = 30.0; // fps
    private static final double MOVE_DURATION = 0.5;        // secs
    private static final double ROTATE_DURATION = 1.0;      // secs
    private static final double TROPHY_ZOOM_IN_DURATION = 3.0;  // secs
    private static final double TROPHY_ZOOM_OUT_DURATION = 0.5; // secs
    private static final double RESET_DURATION = 1.0;           // secs
    
    private static final double INITIAL_SPEED = 1.0;
    
    private Display display;
    
    private MazeReader reader = null;
    private Maze maze = null;

    private IntPoint robotPosition;
    private IntPoint northWestBound = null;
    private IntPoint southEastBound = null;
    private int rotation;
    private double speed = INITIAL_SPEED;
    
    private DisplayGrid grid;
    private DisplayImage robotImage;
    private DisplayImage trophyImage;
    
    private AnimPoint animRobotPosition;
    private AnimatedProperty gridAngleProperty;
    
    private double duration(double baseDuration)
    {
        return baseDuration / (speed / 2.0 + 0.5);
    }

    /** Represents a possible move within the maze. */
    private abstract class Move
    {
        private IntPoint moveDelta;
        private AnimPoint budgeDelta;
        private AnimPoint inverseBudgeDelta;
        
        protected Move(IntPoint moveDelta)
        {
            this.moveDelta = moveDelta;
            this.budgeDelta = new AnimPoint(moveDelta).scale(BUDGE_DISTANCE);
            this.inverseBudgeDelta = new AnimPoint(moveDelta).scale(-BUDGE_DISTANCE);
        }
        
        public abstract boolean isValid();
    
        public void move() throws InterruptedException
        {
            if(isValid())
            {
                robotPosition = robotPosition.plus(moveDelta);
                
                // Produces an animation of the robot successfully moving to a new position.
                makeAnimator(duration(MOVE_DURATION), ANIMATION_FRAMERATE)
                    .quadratic()
                    .of(animRobotPosition.moveBy(moveDelta))
                    .animate();
            }
            else
            {
                // Produce an animation of the robot attempting and failing to move, because 
                // there's a wall in the way. The "budge" animation has the robot moving a short 
                // distance towards the wall and then rebounding back to its original position.
                
                makeAnimator(duration(MOVE_DURATION / 2.0), ANIMATION_FRAMERATE)
                    .quadratic()
                    .of(animRobotPosition.moveBy(budgeDelta))
                    .animate();
                    
                makeAnimator(duration(MOVE_DURATION / 2.0), ANIMATION_FRAMERATE)
                    .quadratic()
                    .of(animRobotPosition.moveBy(inverseBudgeDelta))
                    .animate();
            }
            checkInteraction();
        }
    }
    
    private Move[] moveArray = 
    {
        new Move(IntPoint.xy(0, -1)) // NORTH
        {
            @Override public boolean isValid() 
            {
                return robotPosition.below(northWestBound) && 
                       !maze.horizWall(robotPosition);
            }
        },
        
        new Move(IntPoint.xy(1, 0)) // EAST
        {
            @Override public boolean isValid()
            {
                return robotPosition.leftOf(southEastBound) && 
                       !maze.vertWall(robotPosition.right());
            }
        },
        
        new Move(IntPoint.xy(0, 1)) // SOUTH
        {
            @Override public boolean isValid()
            {
                return robotPosition.above(southEastBound) && 
                       !maze.horizWall(robotPosition.down());
            }
        },
        
        new Move(IntPoint.xy(-1, 0)) // WEST
        {
            @Override public boolean isValid()
            {
                return robotPosition.rightOf(northWestBound) && 
                       !maze.vertWall(robotPosition);
            }
        }
    };
    

    public MazeScenario()
    {
    }
    
    @ListInput(
        order = 1,
        value = {"Maze 1", "Maze 2", "Maze 3"},
        data = {"original.maze", "diamond.maze", "new.maze"})
    public void setMaze(String filename) throws ScenarioSetupException
    {
        reader = new MazeReader(getResources());
        maze = reader.readMaze(filename);
        
        display = getDisplay();
        display.reset();
        display.setBgColour(maze.getBgColour());
        
        IntPoint size = maze.getSize();
        northWestBound = IntPoint.ORIGIN;
        southEastBound = size.up().left();        
        
        IntPoint initialRobotPos = maze.getInitialRobotPosition();
        IntPoint trophyPos = maze.getTrophyPosition();
        
        grid = makeGrid(size);
        grid.setOffset(-0.5, -0.5);
        
        robotImage = makeImage("Blue-Robot.png").at(initialRobotPos).zOrder(2);
        trophyImage = makeImage("trophy-remix-by-monsterbraingames.png").at(trophyPos).zOrder(1);

        display.offset().set(-0.5, -0.5);
        display.size().set(size);
        display.setLineThickness(5);
        
        Color wallColour = maze.getWallColour();
        
        for(IntPoint wallPos : maze.getHorizWalls())
        {
            grid.setHorizLines(wallPos, wallPos, wallColour);
        }
        
        for(IntPoint wallPos : maze.getVertWalls())
        {
            grid.setVertLines(wallPos, wallPos, wallColour);
        }

        robotPosition = initialRobotPos;
        animRobotPosition = robotImage.position();
        gridAngleProperty = new AnimatedProperty(display, "gridAngle");
        
        rotation = maze.getInitialRotation();
        display.setGridAngle((double)rotation * (Math.PI / 2.0));
        
        for(RotationTrap trap : maze.getRotationTraps())
        {
            makeImage(trap.getType().getImageName()).at(trap.getPosition()).zOrder(0);
        }
    }
    
    @DoubleInput(label = "Speed", order = 2, value = 1, min = 0, max = 10)
    public void setSpeed(double speed)
    {
        this.speed = speed;
    }
    
    /** Retrieve the initial Python code to be placed in the editor. */
    @Override
    public String defaultCode()
    {
        return "south(2)\nwest(1)\n";
    }
    
    /** Initialises the maze resources and state. */
    @Override
    public void setup() throws ScenarioSetupException, IOException
    {
        setMaze("original.maze");
    }
    
    /** Returns the maze to its initial state. */
    @Override
    public void reset() throws InterruptedException
    {
        IntPoint initialRobotPos = maze.getInitialRobotPosition();
        int initialRotation = maze.getInitialRotation();
    
        if(!robotPosition.equals(initialRobotPos) || rotation != initialRotation)
        {
            makeAnimator(duration(RESET_DURATION), ANIMATION_FRAMERATE)
                .quadratic()
                .of(animRobotPosition.moveTo(initialRobotPos))
                .of(gridAngleProperty.moveTo((double)initialRotation * (Math.PI / 2.0)))
                .animate();
                
            robotPosition = initialRobotPos;
            rotation = initialRotation;
        }
    }
    
    
    /** 
     * <p>Determines whether something is supposed to happen at a robot's new position, and enacts
     * any appropriate action. This may include rotating the maze, if we've hit a rotation trap, or
     * winning the game if we've run into the trophy.</p>
     */
    private void checkInteraction() throws InterruptedException
    {
        // Check if there's a rotation trap here...
        int rotationDelta = maze.rotationTrap(robotPosition).getDelta();
        if(rotationDelta != 0)
        {
            makeAnimator(duration(ROTATE_DURATION), ANIMATION_FRAMERATE)
                .quadratic()
                .of(gridAngleProperty.moveBy(-((double)rotationDelta * Math.PI / 2.0)))
                .animate();            
            rotation += rotationDelta % moveArray.length;
            if(rotation < 0)
            {
                rotation += moveArray.length;
            }
        }
        
        // Now check if we've reached the end
        IntPoint trophyPos = maze.getTrophyPosition();
        if(robotPosition.equals(trophyPos))
        {
            IntPoint mazeSize = maze.getSize();
            int nRows = mazeSize.getRow();
            int nCols = mazeSize.getCol();
            
            makeAnimator(duration(TROPHY_ZOOM_IN_DURATION), ANIMATION_FRAMERATE)
                .quadratic()
                .of(trophyImage.size().moveTo(mazeSize))
                .of(trophyImage.position().moveTo((double)nCols * 0.5 - 0.5,
                                                  (double)nRows * 0.5 - 0.5))
                .animate();
                
            makeAnimator(duration(TROPHY_ZOOM_OUT_DURATION), ANIMATION_FRAMERATE)
                .quadratic()
                .of(trophyImage.size().moveTo(1.0, 1.0))
                .of(trophyImage.position().moveTo(trophyPos))
                .animate();
        }
    }
    
    /** 
     * <p>Moves the robot a given number of squares in the given direction, where the direction is
     * affected by the current rotation of the maze.</p>
     *
     * @param direction The angle in which to move, as a value from 0--3, representing an angle in
     *        90-degree clockwise increments, starting at north.
     *
     * @param distance The number of squares to move (or attempt to move).
     */     
    private void move(int direction, int distance) throws InterruptedException
    {
        for(int i = 0; i < distance; i++)
        {
            moveArray[(direction + rotation) % moveArray.length].move();
        }
    }
    
    @Embed
    public void north(int distance) throws InterruptedException
    {
        move(0, distance);
    }
    
    @Embed
    public void east(int distance) throws InterruptedException
    {
        move(1, distance);
    }
    
    @Embed
    public void south(int distance) throws InterruptedException
    {
        move(2, distance);
    }
    
    @Embed
    public void west(int distance) throws InterruptedException
    {
        move(3, distance);
    }
}
