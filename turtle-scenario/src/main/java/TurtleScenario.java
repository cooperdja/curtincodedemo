import edu.curtin.cs.codedemo.scenario.*;
import edu.curtin.cs.codedemo.scenario.display.*;
import edu.curtin.cs.codedemo.scenario.input.*;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

@ScenarioDetails(name     = "Turtle",
                 author   = "David Cooper",
                 iconFile = "turtlegraphics_screenshot.png",
                 helpFile = "turtle.html")

public class TurtleScenario extends Scenario
{
    private static final int CANVAS_WIDTH = 1024;
    private static final int CANVAS_HEIGHT = 1024;
    private static final Color BACKGROUND_COLOUR = new Color(0, 0, 0, 0x80); //Color.BLACK;
    private static final Color INITIAL_COLOUR = Color.WHITE;
    private static final double INITIAL_X = (double)CANVAS_WIDTH / 2.0;
    private static final double INITIAL_Y = (double)CANVAS_WIDTH / 2.0;
    private static final double INITIAL_ANGLE = Math.PI / 2.0;
    private static final int INITIAL_DELAY = 100;
    
    private static final double LENGTH_SCALE = 10.0;
    private static final BasicStroke STROKE = new BasicStroke(5.0f);
    
    private BufferedImage canvas;
    private Graphics2D drawer;
    
    private double x;
    private double y;
    private double angle;
    private int delay;
    
    public TurtleScenario()
    {
    }
    
    @IntInput(label = "Draw delay (ms)", value = INITIAL_DELAY, min = 0, step = 10)
    public void setDelay(int delay) 
    {
        this.delay = delay;
//         System.out.println(delay);
    }
    
    
    @Override
    public String defaultCode()
    {
//         return "colour(\"red\")\nspeed(10)\nfor i in range(10):\n    draw(30)\n    rotate(45)\n    draw(15)\n    rotate(120)\n    draw(20)\n\ncolour(\"yellow\")\nmove(20)\nrotate(random(-90,90))\ndraw(random(20,60))";
        return "colour(\"red\")\nfor i in range(10):\n    draw(30)\n    rotate(45)\n    draw(15)\n    rotate(120)\n    draw(20)\n\ncolour(\"yellow\")\nmove(20)\nrotate(random(-90,90))\ndraw(random(20,60))";
    }
    
    @Override
    public void setup() throws IOException
    {
        Display display = getDisplay();
    
        canvas = new BufferedImage(CANVAS_WIDTH, CANVAS_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        drawer = canvas.createGraphics();
        display.offset().set(0.0, 0.0);
        display.size().set(1.0, 1.0);
        makeImage(canvas).at(0.5, 0.5).size(1.0, 1.0);
        setDelay(INITIAL_DELAY);
        
        reset();
    }
    
    @Override
    public void reset()
    {
        x = INITIAL_X;
        y = INITIAL_Y;
        angle = INITIAL_ANGLE;
//         delay = INITIAL_DELAY;
        
//         drawer.setColor(BACKGROUND_COLOUR);
//         drawer.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        drawer.setBackground(BACKGROUND_COLOUR);
        drawer.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        
        drawer.setColor(INITIAL_COLOUR);
        drawer.setStroke(STROKE);
        getDisplay().update();
    }    
    
    @Embed
    public void draw(double length) throws InterruptedException
    {
        double originalX = x, originalY = y;
        length *= LENGTH_SCALE;
        
        x = originalX + Math.cos(angle) * length;
        y = originalY - Math.sin(angle) * length;
        drawer.draw(new Line2D.Double(originalX, originalY, x, y));
        
        getDisplay().update();
        Thread.currentThread().sleep((long)delay);
    }
    
    @Embed
    public void move(double length) throws InterruptedException
    {
        length *= LENGTH_SCALE;
        x += Math.cos(angle) * length;
        y += Math.sin(angle) * length;
    }
    
    @Embed
    public void rotate(double angleDegrees) throws InterruptedException
    {
        angle += angleDegrees * (Math.PI / 180.0);
    }
    
    @Embed
    public void colour(String colourName) throws IllegalArgumentException
    {
        try
        {
            drawer.setColor((Color)Color.class.getField(colourName).get(null));
        }
        catch(ClassCastException | ReflectiveOperationException e)
        {
            throw new IllegalArgumentException("\"" + colourName + "\" is not a recognised colour", e);
        }
    }
    
//     @Embed
//     public void speed(double speed) throws IllegalArgumentException
//     {
//         if(speed <= 0.0)
//         {
//             throw new IllegalArgumentException("Speed must be positive");
//         }
//         delay = (long)(1000.0 / speed);
//     }
    
    @Embed
    public double random(double from, double to)
    {
        return Math.random() * (to - from) + from;
    }
}
