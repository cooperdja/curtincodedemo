package edu.curtin.cs.codedemo.controller;

public class NotRunningException extends Exception
{
    public NotRunningException(String msg)
    {
        super(msg);
    }
}
