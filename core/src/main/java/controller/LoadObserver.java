package edu.curtin.cs.codedemo.controller;
import edu.curtin.cs.codedemo.scenario.Scenario;

public interface LoadObserver
{
    void scenarioLoaded(Scenario scenario);
}
