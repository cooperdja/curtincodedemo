package edu.curtin.cs.codedemo.controller;
import edu.curtin.cs.codedemo.ui.*;
import edu.curtin.cs.codedemo.scenario.*;
import edu.curtin.cs.codedemo.scenario.display.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class ControllerImpl implements Controller
{
    private ScenarioLoader loader;
    private Scenario scenario;
    private Display display;
    private CodeEditor editor;
    private ScenarioRunner runner;
    private DisplayFactory factory;
    
    private Set<LoadObserver> loadObservers = new HashSet<>();

    public ControllerImpl(ScenarioLoader loader, ScenarioRunner runner, 
                          Display display, CodeEditor editor, DisplayFactory factory)
    {
        this.loader = loader;
        this.runner = runner;
        this.display = display;
        this.editor = editor;
        this.factory = factory;
    }
    
    public void addLoadObserver(LoadObserver ob)
    {
        this.loadObservers.add(ob);
    }

    @Override
    public synchronized void loadScenario(File scenarioFile) throws ScenarioLoadException
    {
        scenario = loader.load(scenarioFile);
        display.reset();
        scenario.setDisplay(display);
        scenario.setFactory(factory);
        try
        {
            scenario.setup();
        }
        catch(IOException e)
        {
            String msg = String.format(
                "Error while initialising scenario -- unable to load resources(s): %s", 
                e.getMessage());
            throw new ScenarioLoadException(msg, e);
        }
        catch(RuntimeException e)
        {
            String msg = String.format(
                "Error while initialising scenario -- probable bug in scenario: %s", 
                e.getMessage());
            throw new ScenarioLoadException(msg, e);
        }
        
        for(LoadObserver ob : loadObservers)
        {
            ob.scenarioLoaded(scenario);
        }
    }
    
    @Override
    public synchronized void startScenario() throws AlreadyRunningException
    {
        runner.start(scenario, editor.getCode());
    }
    
    @Override
    public void stopScenario() throws NotRunningException
    {
        runner.stop();
    }
    
    @Override
    public String getHelpText() throws ScenarioLoadException
    {
        String helpFilename = scenario.getHelpFilename();
        if(helpFilename == null || "".equals(helpFilename))
        {
            return "<html><body><p>No help text provided.</p></body></html>";
        }
        
        try
        {
            return scenario.getResources().getTextFile(helpFilename);
        }
        catch(IOException e)
        {
            throw new ScenarioLoadException("Could not load help text", e);
        }
    }
        
    @Override
    public Scenario getScenario()
    {
        return scenario;
    }
}
