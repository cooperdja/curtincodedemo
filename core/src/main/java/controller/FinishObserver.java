package edu.curtin.cs.codedemo.controller;

public interface FinishObserver
{
    void scenarioFinished();
}
