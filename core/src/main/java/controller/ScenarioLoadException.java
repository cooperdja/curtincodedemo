package edu.curtin.cs.codedemo.controller;

public class ScenarioLoadException extends Exception
{
    public ScenarioLoadException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
    
    public ScenarioLoadException(String msg)
    {
        super(msg);
    }
}
