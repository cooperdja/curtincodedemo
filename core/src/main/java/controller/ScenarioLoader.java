package edu.curtin.cs.codedemo.controller;
import edu.curtin.cs.codedemo.scenario.*;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;

public class ScenarioLoader
{
    private static final Logger logger = Logger.getLogger(ScenarioLoader.class.getName());
    private static final String SCENARIO_MANIFEST_ATTRIBUTE = "Scenario-Class";
    
    private Map<File,Scenario> scenarioCache = new HashMap<>();
    
    public ScenarioLoader() {}
    
    public List<Scenario> loadAll(File scenarioDirectory)
    {
        FileFilter filter = new FileFilter()
        {
            @Override
            public boolean accept(File pathname)
            {
                return pathname.isFile() &&
                       pathname.canRead() &&
                       pathname.getName().endsWith(".scenario.jar");
            }
        };
    
        List<Scenario> list = new LinkedList<>();
        for(File f : scenarioDirectory.listFiles(filter))
        {
            try
            {
                list.add(load(f));
            }
            catch(ScenarioLoadException e)
            {
                logger.log(
                    Level.WARNING, 
                    String.format(
                        "Failed to load scenario file '%s': %s'",
                        f.getName(), e.getMessage()),
                    e
                );
            }
        }
        return list;
    }
    
    public List<Scenario> loadAll()
    {
        try
        {
            return loadAll(
                new File(
                    getClass().getProtectionDomain().getCodeSource().getLocation().toURI()
                ).getParentFile()
            );
        }
        catch(URISyntaxException e)
        {
            // Could in principle be thrown if getLocation gives a faulty URL, rejected toURI but 
            // not the URL constructor. 
            String msg = "Failed to find scenario jar files: " + e.getMessage();
            logger.log(Level.SEVERE, msg, e);
            throw new AssertionError(msg, e);
        }
    }
    
    public Scenario load(File scenarioFile) throws ScenarioLoadException
    {
        Scenario scenario = scenarioCache.get(scenarioFile);
        if(scenario == null)
        {
            try
            {
                ScenarioResources resources;
                String scenarioClassName = null;
                URL scenarioUrl;
                
                try
                {
                    scenarioUrl = new URL("jar", "", scenarioFile.toURI().toURL() + "!/");
                    JarURLConnection jarConn = (JarURLConnection) (scenarioUrl.openConnection());
                    scenarioClassName = jarConn.getMainAttributes().getValue(SCENARIO_MANIFEST_ATTRIBUTE);
                    resources = new ScenarioResources(jarConn.getJarFile());
                }
                catch(MalformedURLException | ClassCastException e)
                {
                    String msg = "Failed to access scenario jar file: " + e.getMessage();
                    logger.log(Level.SEVERE, msg, e);
                    throw new AssertionError("Internal error: " + msg, e);
                }
                catch(IOException e)
                {
                    String msg = String.format(
                        "Could not load scenario JAR file \"%s\": %s", 
                        scenarioFile, e.getMessage());
                    logger.log(Level.INFO, msg, e);
                    throw new ScenarioLoadException(msg, e);
                }
                
                if(scenarioClassName == null)
                {
                    String msg = String.format(
                        "\"%s\" is not a valid scenario JAR file -- missing \"%s\" manifest attribute.",
                        scenarioFile, SCENARIO_MANIFEST_ATTRIBUTE);
                    logger.info(msg);
                    throw new ScenarioLoadException(msg);
                }
                
                try
                {
                    ClassLoader loader = new URLClassLoader(new URL[]{scenarioUrl});
                    Class<?> scenarioClass = loader.loadClass(scenarioClassName);
                    
                    scenario = (Scenario) (scenarioClass.newInstance());
                    scenario.setFile(scenarioFile);
                    scenario.setResources(resources);
                    scenarioCache.put(scenarioFile, scenario);
                }
                catch(ClassNotFoundException e)
                {                
                    String msg = String.format(
                        "\"%s\" is not a valid scenario JAR file -- \"%s\" manifest attribute indicates a non-existent class \"%s\" (%s)", 
                        scenarioFile, SCENARIO_MANIFEST_ATTRIBUTE, scenarioClassName, e.getMessage());
                    logger.log(Level.INFO, msg, e);
                    throw new ScenarioLoadException(msg, e);
                }
                catch(ExceptionInInitializerError e)
                {
                    String msg = String.format(
                        "Error in scenario -- class %s generated an exception during initialisation: %s",
                        scenarioClassName, e.getCause().getMessage());
                    logger.log(Level.INFO, msg, e);
                    throw new ScenarioLoadException(msg, e);
                }
                catch(InstantiationException | IllegalAccessException e)
                {
                    String msg = String.format(
                        "Error in scenario -- class %s could not be instantiated: %s",
                        scenarioClassName, e.getMessage());
                    logger.log(Level.INFO, msg, e);
                    throw new ScenarioLoadException(msg, e);
                }
                catch(ClassCastException e)
                {
                    String msg = String.format(
                        "Error in scenario -- class %s, specified by the \"%s\" manifest attribute, does not inherit from %s.",
                        scenarioClassName, SCENARIO_MANIFEST_ATTRIBUTE, Scenario.class.getName());
                    logger.log(Level.INFO, msg, e);
                    throw new ScenarioLoadException(msg, e);
                }
            }
            catch(SecurityException e)
            {
                String msg = String.format(
                    "Could not load scenario for security reasons: " + e.getMessage());
                logger.log(Level.INFO, msg, e);
                throw new ScenarioLoadException(msg, e);
            }
        }
        return scenario;
    }
}
