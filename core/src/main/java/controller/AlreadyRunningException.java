package edu.curtin.cs.codedemo.controller;

public class AlreadyRunningException extends Exception
{
    public AlreadyRunningException(String msg)
    {
        super(msg);
    }
}
