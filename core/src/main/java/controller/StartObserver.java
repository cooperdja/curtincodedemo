package edu.curtin.cs.codedemo.controller;
import edu.curtin.cs.codedemo.scenario.Scenario;

public interface StartObserver
{
    void scenarioStarted(Scenario scenario);
}
