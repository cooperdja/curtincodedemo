package edu.curtin.cs.codedemo.controller;
import edu.curtin.cs.codedemo.scenario.*;

import org.python.util.PythonInterpreter;
import org.python.core.*;

import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.*;

public class ScenarioRunner
{
    private static final Logger logger = Logger.getLogger(ScenarioRunner.class.getName());
    
    private static final long LOOPING_DELAY = 2000;
    
    private PythonInterpreter interpreter = null;
    private Set<StartObserver> startObservers = new HashSet<>();
    private Set<RunObserver> runObservers = new HashSet<>();
    private Set<FinishObserver> finishObservers = new HashSet<>();
    private Thread scenarioThread = null;
    
    
    public ScenarioRunner()
    {
        // Attempt to initialise the Python interpreter, but if this fails, mark it as being
        // uninitialised. 
        //
        // The effect is to initialise up-front where possible, since it takes noticeable time, but
        // delay any actual error handling until later when initialisation is re-attempted.
        try
        {
            init();
        }
        catch(PyException e)
        {
            interpreter = null;
            logger.log(Level.SEVERE, "Python initialisation failed", e);
        }
    }
    
    public void addStartObserver(StartObserver ob)
    {
        startObservers.add(ob);
    }
    
    public void addRunObserver(RunObserver ob)
    {
        runObservers.add(ob);
    }
    
    public void addFinishObserver(FinishObserver ob)
    {
        finishObservers.add(ob);
    }
    
    
    private void init() throws PyException
    {
        if(interpreter == null)
        {
            interpreter = new PythonInterpreter();
            interpreter.exec(
                "import bdb\n" +
                "import sys\n" +
                "import java.lang.Thread\n" +
                "class Debugger(bdb.Bdb):\n" +
                "    def user_line(self, frame):\n" +
                "        callback.atLine(frame.f_lineno - 1)\n" +
                "        java.lang.Thread.sleep(0)\n" +
                "    def user_exception(self, frame, exc_info):\n" +
                "        self.exceptionLine = frame.f_lineno - 1\n" +
    //             "        print('Exception on line ' + str(frame.f_lineno))\n" +
                "debugger = Debugger()"
            );
        }
    }
    
    private static List<Method> getEmbedMethods(Scenario scenario)
    {
        List<Method> methods = new ArrayList<>();
        
        logger.fine("Finding @Embed methods");
        for(Method m : scenario.getClass().getMethods())
        {
            if(m.getAnnotation(Embed.class) != null)
            {
                methods.add(m);
            }
        }
        return methods;
    }
    
    public void start(final Scenario scenario, final String codeText, final boolean loop) 
        throws AlreadyRunningException
    {
        logger.fine("Starting scenario thread");
        if(scenarioThread != null)
        {
            throw new AlreadyRunningException("Scenario already running");
        }
        
        for(StartObserver ob : startObservers)
        {
            ob.scenarioStarted(scenario);
        }
        
        final RunObserver callback = new RunObserver()
        {
            @Override
            public void atLine(int lineN)
            {
                for(RunObserver ob : runObservers)
                {
                    ob.atLine(lineN);
                }
            }
            
            @Override
            public void errorAtLine(int lineN)
            {
                for(RunObserver ob : runObservers)
                {
                    ob.errorAtLine(lineN);
                }
            }
        };
 
        Runnable scenarioTask = new Runnable() 
        {
            @Override
            public void run()
            {
                try
                {
                    do
                    {
                        logger.fine("Resetting scenario");
                        scenario.reset();
                
                        logger.fine("Setting up scenario");
                        
                        // Add the @Embed methods to Python's global scope
                        init();
                        interpreter.exec("scope = {}");
                        interpreter.set("callback", callback);
                        interpreter.set("scenarioObj", scenario);
                        for(Method m : getEmbedMethods(scenario))
                        {
                            String name = m.getName();
                            interpreter.exec(String.format("scope['%s'] = scenarioObj.%s", name, name));
                        }
                        
                        interpreter.set("codeText", codeText);
                        
                        logger.fine("Running user code");
                        
                        try
                        {
                            interpreter.exec(
                                "try:\n" +
                                "    debugger.run(codeText, scope)\n" +
                                "except Exception as e:\n" +
                                "    callback.errorAtLine(debugger.exceptionLine)"
                            );
                        }
                        catch(PyException pyEx)
                        {
                            Object javaEx = pyEx.value.__tojava__(InterruptedException.class);
                            if(javaEx != null)
                            {
                                throw (InterruptedException) javaEx;
                            }
                            
                            String msg = "Exception running Python code: " + pyEx.value;
                            logger.log(Level.INFO, msg, pyEx);
                        }
                        
                        if(loop)
                        {
                            Thread.sleep(LOOPING_DELAY);
                        }
                    }
                    while(loop);
                }
                catch(InterruptedException e)
                {
                    logger.log(Level.FINE, "Scenario interrupted", e);
                }
                finally
                {
                    logger.fine("Scenario finished");
                    for(FinishObserver ob : finishObservers)
                    {
                        ob.scenarioFinished();
                    }
                    scenarioThread = null;
                }
            }
        };
        
        scenarioThread = new Thread(scenarioTask, "scenario-thread");
        scenarioThread.start();
    }
    
    public void stop() throws NotRunningException
    {
        if(scenarioThread == null)
        {
            throw new NotRunningException("Scenario not running");
        }
        
        scenarioThread.interrupt();
    }
    
}
