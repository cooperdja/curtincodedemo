package edu.curtin.cs.codedemo.controller;
import edu.curtin.cs.codedemo.scenario.Scenario;
import java.io.File;

public interface Controller
{
    void loadScenario(File scenarioFile) throws ScenarioLoadException;
    void startScenario() throws AlreadyRunningException;
    void stopScenario() throws NotRunningException;
    String getHelpText() throws ScenarioLoadException;
    Scenario getScenario();
}
