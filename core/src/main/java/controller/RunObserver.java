package edu.curtin.cs.codedemo.controller;

public interface RunObserver
{
    void atLine(int lineN);
    void errorAtLine(int lineN);
}
