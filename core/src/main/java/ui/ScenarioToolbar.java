package edu.curtin.cs.codedemo.ui;

import edu.curtin.cs.codedemo.controller.LoadObserver;
import edu.curtin.cs.codedemo.scenario.*;
import edu.curtin.cs.codedemo.scenario.input.*;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

import java.util.logging.*;

public class ScenarioToolbar extends InputProvider
{
    private static final Color TRANSPARENT = new Color(0, 0, 0, 0);
    
    private JPanel toolbar;
    private Map<Integer,List<JComponent>> newInputs = new TreeMap<>();

    public ScenarioToolbar()
    {
        toolbar = new JPanel();
        toolbar.setOpaque(false);
        toolbar.setBackground(TRANSPARENT);
        toolbar.setFocusable(false);
    }
    
    public void addTo(final JToolBar container)
    {
        container.add(this.toolbar);
        
        container.addPropertyChangeListener(
            "orientation",
            new PropertyChangeListener()
            {
                @Override
                public void propertyChange(PropertyChangeEvent e)
                {
                    if(((Integer)e.getNewValue()).intValue() == JToolBar.VERTICAL)
                    {
                        toolbar.setLayout(new BoxLayout(toolbar, BoxLayout.Y_AXIS));
                    }
                    else
                    {
                        toolbar.setLayout(new BoxLayout(toolbar, BoxLayout.X_AXIS));
                    }
                }
            }
        );
    }

    private void addNewInput(InputHandler handler, JComponent widget)
    {
        String label = handler.getLabel();
        
        if(label != null)
        {
            JPanel labelledWidget = new JPanel();
            labelledWidget.setOpaque(false);
            labelledWidget.setBackground(TRANSPARENT);
            labelledWidget.add(new JLabel(label));
            labelledWidget.add(widget);
            widget = labelledWidget;
        }
        
        int order = handler.getOrder();
        List<JComponent> componentList = newInputs.get(order);
        if(componentList == null)
        {
            componentList = new ArrayList<>();
            newInputs.put(order, componentList);
        }
        componentList.add(widget);
    }
    
    @Override
    protected InputInitialiser[] getInputInitialisers()
    {
        return new InputInitialiser[] {
            new InputInitialiser.List()
            {
                @Override
                public void connectHandler(final ListInputHandler handler)
                {
                    final JComboBox<String> comboBox = new JComboBox<>();
                    final Map<String,String> inputMap = handler.getInputMap();
                    
                    addNewInput(handler, comboBox);
                    
                    for(String label : inputMap.keySet())
                    {
                        comboBox.addItem(label);
                    }
                    
                    comboBox.addActionListener(new ActionListener()
                    {
                        @Override 
                        public void actionPerformed(ActionEvent event)
                        {
                            try
                            {
                                handler.input(
                                    handler.getInputMap().get(
                                        comboBox.getItemAt(comboBox.getSelectedIndex())));
                            }
                            catch(ScenarioInputSetupException e)
                            {
                                inputError(e);
                            }
                        }
                    });
                }
            },
            
            new InputInitialiser.Int()
            {
                @Override
                public void connectHandler(final IntInputHandler handler)
                {
                    final SpinnerNumberModel spinnerModel = new SpinnerNumberModel(
                        handler.getDefaultValue(), handler.getMin(), handler.getMax(), handler.getStep());
                        
                    JSpinner spinner = new JSpinner(spinnerModel);
                    addNewInput(handler, spinner);
                    
                    spinner.addChangeListener(new ChangeListener()
                    {
                        @Override
                        public void stateChanged(ChangeEvent event)
                        {
                            try
                            {
                                handler.input(spinnerModel.getNumber().intValue());
                            }
                            catch(ScenarioInputSetupException e)
                            {
                                inputError(e);
                            }
                        }
                    });
                }
            },
            
            new InputInitialiser.Double()
            {
                @Override
                public void connectHandler(final DoubleInputHandler handler)
                {
                    final SpinnerNumberModel spinnerModel = new SpinnerNumberModel(
                        handler.getDefaultValue(), handler.getMin(), handler.getMax(), handler.getStep());
                        
                    JSpinner spinner = new JSpinner(spinnerModel);
                    addNewInput(handler, spinner);
                    
                    spinner.addChangeListener(new ChangeListener()
                    {
                        @Override
                        public void stateChanged(ChangeEvent event)
                        {
                            try
                            {
                                handler.input(spinnerModel.getNumber().doubleValue());
                            }
                            catch(ScenarioInputSetupException e)
                            {
                                inputError(e);
                            }
                        }
                    });
                }
            }
            
        };
    }
    
    private void inputError(ScenarioInputSetupException e)
    {
        JOptionPane.showMessageDialog(
            toolbar,
            String.format(
                "<html><p>%s</p></html>", 
                e.getMessage()),
            "Scenario Error",
            JOptionPane.ERROR_MESSAGE);
    }
    
    
    @Override
    protected void finaliseSetup()
    {
        toolbar.removeAll();
        toolbar.repaint();
        
        for(List<JComponent> componentList : newInputs.values())
        {
            for(JComponent component : componentList)
            {
                toolbar.add(component);
            }
        }
        
        newInputs.clear();
    }
    
    @Override
    protected void setupFailed(ScenarioInputSetupException e)
    {
        inputError(e);
    }

}
