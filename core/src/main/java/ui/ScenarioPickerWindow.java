package edu.curtin.cs.codedemo.ui;

import edu.curtin.cs.codedemo.scenario.Scenario;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.List;
import java.util.logging.*;
import javax.swing.*;


public class ScenarioPickerWindow extends JDialog
{
    private static final Logger logger = Logger.getLogger(ScenarioPickerWindow.class.getName());
    private static final int ICON_WIDTH = 128;
    private static final int ICON_HEIGHT = 128;

    public ScenarioPickerWindow(final MainWindow parent, List<Scenario> scenarioList)
    {
        super(parent, "Pick Scenario", Dialog.ModalityType.APPLICATION_MODAL);
        
        setResizable(false);
        
        Container contentPane = getContentPane();
        JPanel infoPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel selectionPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel controlPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        
        contentPane.add(infoPane, BorderLayout.PAGE_START);
        contentPane.add(selectionPane, BorderLayout.CENTER);
        contentPane.add(controlPane, BorderLayout.PAGE_END);
        
        infoPane.add(new JLabel("Select a scenario:"));
        
        JButton loadFileButton = new JButton("Load from File...");
        loadFileButton.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent ev)
                {
                    parent.loadScenario();
                    ScenarioPickerWindow.this.setVisible(false);
                }
            }
        );
        
        Action cancel = new AbstractAction("Cancel")
        {
            @Override 
            public void actionPerformed(ActionEvent ev)
            {
                ScenarioPickerWindow.this.setVisible(false);
            }
        };
        
        JButton cancelButton = new JButton(cancel);
        cancelButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
            .put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancel);
        cancelButton.getActionMap().put(cancel, cancel);
        
        controlPane.add(loadFileButton);
        controlPane.add(cancelButton);
        
        selectionPane.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, Color.BLACK));
        
        for(Scenario sc : scenarioList)
        {
            String name = sc.getName();
            String author = sc.getAuthor();
            String iconFile = sc.getIconFilename();
            
            String buttonLabel = "<html><b>" + name + "</b><br>";
            if(author != null)
            {
                buttonLabel += "<i>by " + author + "</i>";
            }
            buttonLabel += "</html>";
            
            ImageIcon icon = null;
            if(iconFile != null)
            {
                try
                {
                    icon = new ImageIcon(sc.getResources()
                                           .getImage(iconFile)
                                           .getScaledInstance(ICON_WIDTH, ICON_HEIGHT, Image.SCALE_SMOOTH));
                }
                catch(IOException e)
                {
                    // Not important enough to display an error message, but definitely log this.
                    logger.log(
                        Level.WARNING, 
                        String.format(
                            "For scenario '%s', could not load icon file '%s'.", 
                            name, iconFile),
                        e
                    );
                }
            }
            
            JButton loadButton = (icon == null) ? new JButton(buttonLabel)
                                                : new JButton(buttonLabel, icon);
            loadButton.setVerticalTextPosition(SwingConstants.BOTTOM);
            loadButton.setHorizontalTextPosition(SwingConstants.CENTER);
            selectionPane.add(loadButton);
            
            class ScenarioActionListener implements ActionListener
            {
                Scenario sc;
                ScenarioActionListener(Scenario sc) { this.sc = sc; }
                
                @Override 
                public void actionPerformed(ActionEvent ev)
                {
                    ScenarioPickerWindow.this.setVisible(false);
                    parent.loadScenario(sc.getFile());
                }
            }
            
            loadButton.addActionListener(new ScenarioActionListener(sc));
        }
        
        pack();
        setLocationRelativeTo(parent);
    }
}
