package edu.curtin.cs.codedemo.ui;

import java.io.File;
import java.net.URL;
import java.net.URISyntaxException;
import java.net.MalformedURLException;
import java.util.logging.*;

import javax.swing.ImageIcon;
import javax.swing.AbstractButton;

public class Resources
{
    private static final Logger logger = Logger.getLogger(Resources.class.getName());
    private URL jarUrl = null;

    public Resources() {}
    
    private URL getJarUrl() throws SecurityException
    {
        if(jarUrl == null)
        {
            jarUrl = getClass().getProtectionDomain().getCodeSource().getLocation();
        }
        return jarUrl;
    }

    /** Retrieves an ImageIcon from within the main .jar file, suitable as a button icon. */
    public ImageIcon getIcon(String filename)
    {
        ImageIcon icon = null;
        try
        {
            icon = new ImageIcon(new URL("jar:" + getJarUrl() + "!/" + filename));
        }
        catch(SecurityException | MalformedURLException e)
        {
            // These exceptions are highly improbable. If they happen, we simply return null.
            logger.log(
                Level.WARNING, 
                String.format("Failed to obtain image file '%s' from jar file.", filename),
                e
            );
        }
        return icon;
    }
    
    /** Convenience method to add an icon to a button. */
    public void setIcon(AbstractButton button, String filename)
    {
        ImageIcon icon = getIcon(filename);
        if(icon != null)
        {
            button.setIcon(icon);
        }
    }
    
    /** Retrieves the directory in which the main .jar file resides. */
    public File getLocalDirectory()
    {
        File dir = null;
        try
        {
            dir = new File(getJarUrl().toURI()).getParentFile();
        }
        catch(SecurityException | URISyntaxException e)
        {
            // These exceptions are highly improbable. If they happen, we'll return null (upon 
            // which JFileChooser shows the user's home directory).
            logger.log(
                Level.WARNING, 
                String.format("Failed to determine location of jar file '%s'.", jarUrl),
                e
            );
        }
        return dir;
    }
}
