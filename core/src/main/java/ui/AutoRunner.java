package edu.curtin.cs.codedemo.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.logging.*;

/**
 *
 * Parts of this code were adapted from / inspired by the following source:
 * https://tips4java.wordpress.com/2008/10/24/application-inactivity/.
 */

public class AutoRunner
{
    private static final Logger logger = Logger.getLogger(AutoRunner.class.getName());
    private static final long DELAY = 30000L; // 5 seconds
    private static final long POLL_FREQUENCY = 1000L;
    
    private ActionListener actionListener;
    private Object lock = new Object();
    private boolean enabled = false;
    private long lastActivityTime;
    private Timer timer = null;
    
    private class TimerCallback extends TimerTask
    {
        @Override
        public void run()
        {
            synchronized(lock)
            {
                if(enabled)
                {
                    long currentTime = System.currentTimeMillis();
                    if(currentTime - lastActivityTime >= DELAY)
                    {
                        logger.fine("Idle threshold reached -- running action");
                        lastActivityTime = currentTime;
                        actionListener.actionPerformed(
                            new ActionEvent(this, ActionEvent.ACTION_PERFORMED, ""));
                    }
                }
            }
        }
    };
    
    private AWTEventListener eventListener = new AWTEventListener()
    {
        @Override
        public void eventDispatched(AWTEvent e)
        {
            synchronized(lock)
            {
                if(enabled)
                {
                    lastActivityTime = System.currentTimeMillis();
                }
            }
        }
    };

    public AutoRunner() {}
    
    public void enable(ActionListener actionListener)
    {
        synchronized(lock)
        {
            if(!enabled)
            {
                enabled = true;
                this.actionListener = actionListener;
                
                lastActivityTime = System.currentTimeMillis();
                timer = new Timer("autorunner");
                timer.scheduleAtFixedRate(new TimerCallback(), POLL_FREQUENCY, POLL_FREQUENCY);
                
                Toolkit.getDefaultToolkit().addAWTEventListener(
                    eventListener, 
                    AWTEvent.KEY_EVENT_MASK | 
                    AWTEvent.MOUSE_MOTION_EVENT_MASK | 
                    AWTEvent.MOUSE_EVENT_MASK);
                    
                logger.fine("AutoRunner enabled");
            }
        }
    }
    
    public void disable()
    {
        synchronized(lock)
        {
            if(enabled)
            {
                enabled = false;
                this.actionListener = null;
                
                timer.cancel();
                timer = null;
                
                Toolkit.getDefaultToolkit().removeAWTEventListener(eventListener);
                
                logger.fine("AutoRunner disabled");
            }
        }
    }
}
