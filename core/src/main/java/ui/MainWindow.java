package edu.curtin.cs.codedemo.ui;
import edu.curtin.cs.codedemo.ui.display.*;
import edu.curtin.cs.codedemo.scenario.Scenario;
import edu.curtin.cs.codedemo.controller.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.*;

import java.io.File;
import java.util.logging.*;


public class MainWindow extends JFrame implements FinishObserver
{
    private static final Logger logger = Logger.getLogger(MainWindow.class.getName());

    private Color START_COLOUR = new Color(0x80, 0xff, 0x80);
    private Color STOP_COLOUR = new Color(0xff, 0x80, 0x80);
    
    private Action startAction;
    private Action loopRunAction;
    private Action stopAction;
    private JCheckBoxMenuItem autorunMenuItem;
    
    private Window scenarioPicker;
    private JFileChooser scenarioFilePicker;
    private Controller controller = null;
    
    private Action loadScenarioAction;
    private Action loadScenarioFileAction;
    

    public MainWindow(DisplayPanel display, final CodeEditor editor, ScenarioToolbar toolbar,
                      final AutoRunner autoRunner, final HelpWindow helpWindow, Resources resources)
    {
        super("Curtin Code Demo");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        final JButton menuButton = new JButton("File");
        JButton startButton = new JButton("Start");
        JButton stopButton = new JButton("Stop");
        JButton helpButton = new JButton("Help");
        
        this.scenarioPicker = null;
        
        JToolBar fullToolbar = new JToolBar();
        fullToolbar.add(menuButton);        
        fullToolbar.addSeparator();
        fullToolbar.add(startButton);
        fullToolbar.add(stopButton);
        fullToolbar.addSeparator();
        toolbar.addTo(fullToolbar);
        fullToolbar.add(Box.createHorizontalGlue());
        fullToolbar.add(helpButton);
        
        for(JComponent nonFocusable : new JComponent[]{ 
            fullToolbar, menuButton, startButton, stopButton, helpButton })
        {
            nonFocusable.setFocusable(false);
        }
        
        final JPopupMenu menu = new JPopupMenu();
        
        menuButton.setAction(new AbstractAction("File")
        {
            {
                putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                    KeyEvent.VK_F, KeyEvent.ALT_DOWN_MASK));
            }
        
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(menu.isVisible())
                {
                    menu.setVisible(false);
                }
                else
                {
                    menu.show(menuButton, 0, menuButton.getSize().height);
                }
            }
        });
        
        loadScenarioAction = new AbstractAction("Load Scenario...")
        {
            {
                setEnabled(false);
                putValue(MNEMONIC_KEY, KeyEvent.VK_L);
                putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                    KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK));
            }
            
            @Override 
            public void actionPerformed(ActionEvent ev)
            {
                logger.fine("Showing scenario picker");
                scenarioPicker.setVisible(true);
            }
        };
        
        loadScenarioFileAction = new AbstractAction("Load Scenario from File...")
        {
            {
                setEnabled(false);
                putValue(MNEMONIC_KEY, KeyEvent.VK_F);
            }
            
            @Override 
            public void actionPerformed(ActionEvent ev)
            {
                logger.fine("Loading scenario");
                loadScenario();
            }
        };
        
        Action openScriptAction = new AbstractAction("Open Script...")
        {
            {
                putValue(MNEMONIC_KEY, KeyEvent.VK_O);
                putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                    KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));
            }
            
            @Override
            public void actionPerformed(ActionEvent event)
            {
                editor.openFile();
            }
        };
        
        Action saveScriptAction = new AbstractAction("Save Script...")
        {
            {
                putValue(MNEMONIC_KEY, KeyEvent.VK_S);
                putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                    KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
            }
            
            @Override
            public void actionPerformed(ActionEvent event)
            {
                editor.saveFile();
            }
        };
        
        startAction = new AbstractAction("Run")
        {
            {
                putValue(MNEMONIC_KEY, KeyEvent.VK_R);
                putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                    KeyEvent.VK_ENTER, KeyEvent.CTRL_DOWN_MASK));
            }
            
            @Override 
            public void actionPerformed(ActionEvent ev)
            {
                if(controller == null) { return; }
                
                try
                {
                    controller.startScenario();
                    startAction.setEnabled(false);
                    loopRunAction.setEnabled(false);
                    stopAction.setEnabled(true);
                }
                catch(AlreadyRunningException e)
                {
                    logger.log(
                        Level.FINE,
                        "Scenario already running - cannot start again",
                        e
                    );
                }
            }
        };
        
        loopRunAction = new AbstractAction("Run Looped")
        {
            {
                putValue(MNEMONIC_KEY, KeyEvent.VK_P);
                putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                    KeyEvent.VK_ENTER, KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK));
            }
            
            @Override
            public void actionPerformed(ActionEvent ev)
            {
                if(controller == null) { return; }
                
                try
                {
                    controller.startLoopedScenario();
                    startAction.setEnabled(false);
                    loopRunAction.setEnabled(false);
                    stopAction.setEnabled(true);
                }
                catch(AlreadyRunningException e)
                {
                    logger.log(
                        Level.FINE,
                        "Scenario already running - cannot start again",
                        e
                    );
                }
            }
        };
            
        Action setAutorunAction = new AbstractAction("Autorun When Idle")
        {
            {
                putValue(MNEMONIC_KEY, KeyEvent.VK_A);
            }
            
            @Override
            public void actionPerformed(ActionEvent ev)
            {
                if(autorunMenuItem.isSelected())
                {
                    autoRunner.enable(loopRunAction);
                }
                else
                {
                    autoRunner.disable();
                }
            }
        };
        
        stopAction = new AbstractAction("Stop")
        {
            {
                setEnabled(false);
                putValue(MNEMONIC_KEY, KeyEvent.VK_T);
                putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
            }
            
            @Override 
            public void actionPerformed(ActionEvent ev)
            {   
                if(controller == null) { return; }
                
                try
                {
                    controller.stopScenario();
                }
                catch(NotRunningException e)
                {
                    logger.log(Level.INFO, "Scenario not running - cannot stop", e);
                }
            }
        };
        
        Action helpAction = new AbstractAction("Help")
        {
            {
                putValue(MNEMONIC_KEY, KeyEvent.VK_H);
                putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
            }
            
            @Override 
            public void actionPerformed(ActionEvent ev)
            {
                logger.fine("Showing help window");
                helpWindow.showHelp();
            }
        };
        
        Action quitAction = new AbstractAction("Quit")
        {
            {
                putValue(MNEMONIC_KEY, KeyEvent.VK_Q);
                putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(
                    KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK));
            }
            
            @Override
            public void actionPerformed(ActionEvent event)
            {
                dispatchEvent(new WindowEvent(MainWindow.this, WindowEvent.WINDOW_CLOSING));
            }
        };
        
        startButton.setAction(startAction);
        stopButton.setAction(stopAction);
        helpButton.setAction(helpAction);
        
        autorunMenuItem = new JCheckBoxMenuItem(setAutorunAction);
        
        menu.add(loadScenarioAction);
        menu.add(loadScenarioFileAction);
        menu.addSeparator();
        menu.add(openScriptAction);
        menu.add(saveScriptAction);
        menu.addSeparator();
        menu.add(startAction);
        menu.add(loopRunAction);
        menu.add(autorunMenuItem);
        menu.add(stopAction);
        menu.addSeparator();
        menu.add(helpAction);
        menu.addSeparator();
        menu.add(quitAction);
        
                
        Container contentPane = getContentPane();
        
//         Container contentPaneBase = getContentPane();
//         JLabel contentPane = new JLabel(resources.getIcon("curtin-000.png"));
//         contentPaneBase.setLayout(new BorderLayout());
//         contentPaneBase.add(contentPane);
//         contentPane.setLayout(new BorderLayout());
        
//         final ImageIcon bgImage = resources.getIcon("curtin-000.png");
//     
//         JComponent contentPane = new JComponent()
//         {
//             @Override
//             protected void paintComponent(Graphics g)
//             {
//                 g.drawImage(bgImage.getImage(), 0, 0,
//                     Color.BLACK, null);
//                 super.paintComponent(g);
//             }
//         };
//         setContentPane(contentPane);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setOpaque(false);
        splitPane.setBackground(new Color(0, 0, 0, 0));
        editor.addTo(splitPane);
        display.addTo(splitPane);
        
        InputMap inputMap = splitPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = splitPane.getActionMap();
        
        for(Action ac : new Action[]{
            menuButton.getAction(), 
            loadScenarioAction, openScriptAction, saveScriptAction,
            startAction, loopRunAction, stopAction, helpAction, quitAction})
        {
            inputMap.put((KeyStroke)ac.getValue(Action.ACCELERATOR_KEY), ac);
            actionMap.put(ac, ac);
        }
        
        editor.getComponent().addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                if(stopAction.isEnabled())
                {
                    stopAction.actionPerformed(
                        new ActionEvent(MainWindow.this, ActionEvent.ACTION_PERFORMED, ""));
                }
            }
        });
        
        contentPane.add(fullToolbar, BorderLayout.PAGE_START);
        contentPane.add(splitPane, BorderLayout.CENTER);
        
        resources.setIcon(menuButton, "angle-down.png");
        resources.setIcon(startButton, "play-circle-green.png");
        resources.setIcon(stopButton, "stop-circle-red.png");
        resources.setIcon(helpButton, "info-circle-blue.png");
        
        scenarioFilePicker = new JFileChooser(resources.getLocalDirectory());
        scenarioFilePicker.setFileFilter(new FileNameExtensionFilter("Scenario JAR file", "jar"));
        scenarioFilePicker.setDialogTitle("Load Scenario");
        scenarioFilePicker.setApproveButtonText("Load");
        
        pack();
    }

    
    public void setScenarioPicker(Window scenarioPicker)
    {
        this.scenarioPicker = scenarioPicker;
    }
    
    
    public void loadScenario()
    {
        if(scenarioFilePicker.showOpenDialog(MainWindow.this) == JFileChooser.APPROVE_OPTION)
        {
            loadScenario(scenarioFilePicker.getSelectedFile());
        }
    }
    
    /**
     * Loads a scenario. The actual loading is delegated to the controller, but 
     * this method presents any resulting errors to the user.
     *
     * Can be called both internally and externally.
     */
    public void loadScenario(File scenarioFile)
    {
        try
        {
            controller.loadScenario(scenarioFile);
        }
        catch(ScenarioLoadException e)
        {
            logger.log(
                Level.SEVERE,
                String.format("Could not load \"%s\": %s", scenarioFile.getName(), e.getMessage()),
                e
            );
            
            JOptionPane.showMessageDialog(
                MainWindow.this,
                String.format(
                    "<html><p>Could not load \"<em>%s</em>\":</p><p>%s</p></html>", 
                    scenarioFile.getName(), 
                    e.getMessage()),
                "Scenario Load Error",
                JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void setController(final Controller controller)
    {
        if(controller == null)
        {
            throw new IllegalArgumentException();
        }
    
        this.controller = controller;
        
        loadScenarioAction.setEnabled(true);
        loadScenarioFileAction.setEnabled(true);
    }
    
    @Override
    public void scenarioFinished()
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() 
        {
            @Override 
            public void run() 
            {
                startAction.setEnabled(true);
                loopRunAction.setEnabled(true);
                stopAction.setEnabled(false);
                logger.fine("Scenario finished");
            }            
        });
    }
}
