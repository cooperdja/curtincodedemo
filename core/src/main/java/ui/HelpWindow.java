package edu.curtin.cs.codedemo.ui;
import edu.curtin.cs.codedemo.scenario.ScenarioResources;
import edu.curtin.cs.codedemo.controller.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;

import java.io.*;
import java.net.*;
import java.util.logging.*;

public class HelpWindow extends JFrame
{
    private static final Logger logger = Logger.getLogger(HelpWindow.class.getName());
    
    private static final int FONT_SIZE = 20;

    private JEditorPane viewer = new JEditorPane();
    private Controller controller = null;
    private ScenarioResources scenarioResources = null;
    
    private class JarImageView extends ImageView
    {
        private URI uri;
        private Image image = null;
    
        JarImageView(Element elem, URI uri)
        {
            super(elem);
            this.uri = uri;
        }
        
        @Override
        public Image getImage()
        {
            if(image == null)
            {
                try
                {
                    image = scenarioResources.getImage(uri.getPath());
                    
                    // Force synchronous loading (as per documentation in the OpenJDK source code for 
                    // javax.swing.text.html.ImageView.loadImage().
//                     ImageIcon icon = new ImageIcon();
//                     icon.setImage(image);
//                     
//                     AttributeSet attr = getElement().getAttributes();
//                     
//                     int origWidth = icon.getIconWidth();
//                     int origHeight = icon.getIconHeight();
//                     double origAspect = (double)origWidth / (double)origHeight;
//                     
//                     System.out.printf("origWidth=%d, origHeight=%d\n", origWidth, origHeight);
//                                     
//                     int width = getIntAttr(HTML.Attribute.WIDTH, -1);
//                     int height = getIntAttr(HTML.Attribute.HEIGHT, -1);
//                     
//                     System.out.printf("htmlWidth=%d, htmlHeight=%d\n", width, height);
//                     
//                     if(width <= 0 && height <= 0)
//                     {
//                         width = origWidth;
//                         height = origHeight;
//                     }
//                     else if(width <= 0)
//                     {
//                         width = (int)Math.round((double)height * origAspect);
//                     }
//                     else if(height <= 0)
//                     {
//                         height = (int)Math.round((double)width / origAspect);
//                     }
//                     
//                     System.out.printf("width=%d, height=%d\n", width, height);
// 
//                     Toolkit.getDefaultToolkit().prepareImage(image, width, height, null);
//                     Toolkit.getDefaultToolkit().prepareImage(image, -1, 50, null);
                }
                catch(IOException e)
                {
                    image = super.getImage();
                }
            }
            return image;
        }
        
        /**
         * <p>Adapted from the OpenJDK source code for javax.swing.text.html.ImageView.</p>
         */
        private int getIntAttr(HTML.Attribute attrName, int defaultValue) 
        {
            int intVal = defaultValue;
            
            AttributeSet attrSet = getElement().getAttributes();
            if(attrSet.isDefined(attrName)) 
            {
                String strVal = (String)attrSet.getAttribute(attrName);
                if(strVal != null)
                {
                    try
                    {
                        intVal = Math.max(0, Integer.parseInt(strVal));
                    }
                    catch(NumberFormatException e) {} // Use default value
                }
            }
        
            return intVal;
        }        
        
        @Override
        public URL getImageURL()
        {
            try
            {
                // ImageView needs to be told that the image URL is not null, but we don't 
                // necessarily care what it actually is. This will produce a URL of the form 
                // "jar:filename.png".
                return new URL("jar", null, -1, uri.getPath());
            }
            catch(MalformedURLException e)
            {
                // Not expected to happen.                
                logger.log(
                    Level.WARNING,
                    String.format("Could not convert path '%s' to a URL", uri.getPath()),
                    e
                );
                return null;
            }
        }
    }
    
    private class JarHTMLFactory extends HTMLEditorKit.HTMLFactory
    {
        @Override
        public View create(Element elem) 
        {
            if(elem.getName().equalsIgnoreCase("img"))
            {
                String src = (String)elem.getAttributes().getAttribute(HTML.Attribute.SRC);
                try
                {
                    final URI uri = new URI(src);                        
                    if(!uri.isAbsolute())
                    {
                        return new JarImageView(elem, uri);
                    }
                }
                catch(URISyntaxException e)
                {
                    logger.log(
                        Level.WARNING,
                        String.format("Invalid 'src' in <img> HTML element: src=\"%s\"", src),
                        e
                    );
                }
            }

            return super.create(elem);
        }
    }
    
    private HTMLEditorKit editorKit = new HTMLEditorKit()
    {
        private HTMLFactory factory = new JarHTMLFactory();

        @Override
        public ViewFactory getViewFactory() 
        {
            return factory;
        }
    };

    public HelpWindow(Resources resources)
    {
        super("Help (Curtin Code Demo)");
        setSize(800, 600);
        
        Action close = new AbstractAction("Close")
        {
            @Override 
            public void actionPerformed(ActionEvent ev)
            {
                HelpWindow.this.setVisible(false);
            }
        };
        
        JButton closeButton = new JButton(close);
        resources.setIcon(closeButton, "times-circle-black.png");
        
        closeButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
            .put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), close);
        closeButton.getActionMap().put(close, close);
        
        JToolBar buttonBar = new JToolBar();
        buttonBar.setFloatable(false);
        buttonBar.add(Box.createHorizontalGlue());
        buttonBar.add(closeButton);
        
        Container contentPane = getContentPane();
        contentPane.add(new JScrollPane(viewer), BorderLayout.CENTER);
        contentPane.add(buttonBar, BorderLayout.PAGE_END);
        
        viewer.setFont(new Font("sans", Font.PLAIN, FONT_SIZE));
        viewer.setEditable(false);
        viewer.setEditorKit(editorKit);
        
        StyleSheet style = editorKit.getStyleSheet();
        
        Document doc = editorKit.createDefaultDocument();
        viewer.setDocument(doc);
        
        viewer.addHyperlinkListener(new HyperlinkListener()
        {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent event)
            {
                if(HyperlinkEvent.EventType.ACTIVATED.equals(event.getEventType()))
                {
                    try
                    {
                        Desktop desktop = Desktop.getDesktop();
                        desktop.browse(event.getURL().toURI());
                    }
                    catch(UnsupportedOperationException | URISyntaxException | IOException e)
                    {
                        String msg = String.format(
                            "Cannot launch web browser for URL \"%s\"", 
                            event.getURL().toString());
                        logger.log(Level.WARNING, msg, e);
                    }
                }
                else if(HyperlinkEvent.EventType.ENTERED.equals(event.getEventType()))
                {
                    viewer.setToolTipText(event.getURL().toString());
                }
                else if(HyperlinkEvent.EventType.EXITED.equals(event.getEventType()))
                {
                    viewer.setToolTipText(null);
                }                
            }
        });
    }
    
    public void setController(Controller controller)
    {
        this.controller = controller;
    }
    
    public void showHelp()
    {
        if(controller == null)
        {
            String msg = "Call to showHelp() before setProvider()";
            logger.severe(msg);
            throw new IllegalStateException(msg);
        }
        
        try
        {
            scenarioResources = controller.getScenario().getResources();
            viewer.setText(controller.getHelpText());
            viewer.setCaretPosition(0);
            setVisible(true);
        }
        catch(ScenarioLoadException e)
        {
            String msg = "Could not load scenario help text: " + e.getMessage();
            logger.log(Level.SEVERE, msg, e);
            JOptionPane.showMessageDialog(
                null,
                "<html><body><p>" + msg + "</p></body></html>",
                "Scenario Load Error",
                JOptionPane.ERROR_MESSAGE);
        }
    }
}
