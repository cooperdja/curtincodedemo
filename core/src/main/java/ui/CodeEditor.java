package edu.curtin.cs.codedemo.ui;

import edu.curtin.cs.codedemo.controller.*;
import edu.curtin.cs.codedemo.scenario.Scenario;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import javax.swing.undo.*;

import java.util.logging.*;

import java.util.Scanner;
import java.io.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class CodeEditor implements LoadObserver, StartObserver, RunObserver, FinishObserver
{
    private static final Logger logger = Logger.getLogger(CodeEditor.class.getName());

    private static final int DEFAULT_ROWS = 20;
    private static final int DEFAULT_COLS = 15;
    private static final Font FONT = new Font("monospaced", Font.BOLD, 32);
    private static final Color TEXT_COLOUR = Color.WHITE;
    private static final String INDENT = "  ";
    
    private static final Color HIGHLIGHT_COLOUR_RUN = new Color(0x80, 0x80, 0xff);//Color.BLUE.brighter().brighter();
    private static final Color HIGHLIGHT_COLOUR_ERROR = Color.RED;
    
    private JTextArea textArea;
    private JScrollPane scroller;
    private Highlighter highlighter;
    private Highlighter.HighlightPainter runPainter;
    private Highlighter.HighlightPainter errorPainter;
    private Object runHighlight = null;
    private Object errorHighlight = null;
    
    private UndoManager undoer;
    private JFileChooser fileChooser;
    
    private class UndoRecorder implements UndoableEditListener
    {
        private CompoundEdit compoundEdit = null;
        
        public void beginCompoundEdit()
        {
            compoundEdit = new CompoundEdit();
        }
        
        public void endCompoundEdit()
        {
            if(compoundEdit == null)
            {
                throw new IllegalStateException();
            }
            compoundEdit.end();
            undoer.addEdit(compoundEdit);
            compoundEdit = null;
        }
        
        @Override 
        public void undoableEditHappened(UndoableEditEvent event) 
        {
            logger.finer("Undoable edit: " + event.getEdit().getPresentationName());
            
            if(compoundEdit != null)
            {
                compoundEdit.addEdit(event.getEdit());
            }
            else
            {
                undoer.addEdit(event.getEdit());
            }
        }
    }
    
    private UndoRecorder undoRecorder = new UndoRecorder();


    public CodeEditor(final Resources resources) 
    {
        textArea = new JTextArea(DEFAULT_ROWS, DEFAULT_COLS)
        {
            {
                setOpaque(false);
                setBackground(new Color(0, 0, 0, 0));
            }
            
            private ImageIcon bgImage = resources.getIcon("curtin-000.png");
        
            @Override
            protected void paintComponent(Graphics g)
            {
                int imageWidth = bgImage.getIconWidth();
                int imageHeight = bgImage.getIconHeight();
                double imageAspect = (double)imageWidth / (double)imageHeight;                
                
                int taWidth = getWidth();
                int taHeight = getHeight();
                double taAspect = (double)taWidth / (double)taHeight;
                
                if(imageAspect > taAspect)
                {
                    // Image is more short-and-wide than the text area
                    // -> Scale it to fit vertically, and crop horizontally
                    
                    int scaledWidth = (int)Math.round((double)taHeight * imageAspect);
                    
                    g.drawImage(bgImage.getImage(), 
                        (taWidth - scaledWidth) / 2, 0, 
                        scaledWidth, taHeight,
                        Color.BLACK, null);
                }
                else
                {
                    // Image is more tall-and-narrow than the text area
                    // -> Scale it to fit horizontally, and crop vertically
                    
                    int scaledHeight = (int)Math.round((double)taWidth / imageAspect);
                    g.drawImage(bgImage.getImage(), 
                        0, (taHeight - scaledHeight) / 2,
                        taWidth, scaledHeight,
                        Color.BLACK, null);
                }
                
//                 g.drawImage(bgImage.getImage(), 0, 0, Color.BLACK, null);
                super.paintComponent(g);
            }
        };
        
        textArea.setFont(FONT);
        textArea.setForeground(TEXT_COLOUR);
        
        scroller = new JScrollPane(textArea);
        
        highlighter = textArea.getHighlighter();
        runPainter = new DefaultHighlighter.DefaultHighlightPainter(HIGHLIGHT_COLOUR_RUN);
        errorPainter = new DefaultHighlighter.DefaultHighlightPainter(HIGHLIGHT_COLOUR_ERROR);
        
        Document doc = textArea.getDocument();
        ActionMap actionMap = textArea.getActionMap();
        undoer = new UndoManager();
        
        textArea.addFocusListener(new FocusListener()
        {
            @Override
            public void focusGained(FocusEvent e)
            {
                textArea.getCaret().setVisible(textArea.isEditable());
            }
            
            @Override
            public void focusLost(FocusEvent e)
            {
                textArea.getCaret().setVisible(false);
            }
        });
        
        fileChooser = new JFileChooser(resources.getLocalDirectory());
        fileChooser.setFileFilter(new FileNameExtensionFilter("Python source files", "py"));
        
        doc.addDocumentListener(new DocumentListener()
        {
            @Override public void changedUpdate(DocumentEvent e) { highlighter.removeAllHighlights(); }
            @Override public void insertUpdate(DocumentEvent e) { highlighter.removeAllHighlights(); }
            @Override public void removeUpdate(DocumentEvent e) { highlighter.removeAllHighlights(); }
        });
        
        doc.addUndoableEditListener(undoRecorder);

        actionMap.put(
            "Undo",
            new AbstractAction("Undo") 
            {
                @Override 
                public void actionPerformed(ActionEvent evt) 
                {
                    try 
                    {
                        undoer.undo();
                    } 
                    catch (CannotUndoException e) 
                    {
                        logger.log(Level.FINE, "No more undos", e);
                    }
                }
            }
        );

        actionMap.put(
            "Redo",
            new AbstractAction("Redo") 
            {
                @Override
                public void actionPerformed(ActionEvent event) 
                {
                    try 
                    {
                        undoer.redo();
                    } 
                    catch (CannotRedoException e) 
                    {
                        logger.log(Level.FINE, "No more redos", e);
                    }
                }
            }
        );
        
        actionMap.put(
            "Indent",
            new AbstractAction("Indent")
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    try
                    {
                        Document doc = textArea.getDocument();
                        String text = doc.getText(0, doc.getLength());
                        int selectionStart = textArea.getSelectionStart();
                        int selectionEnd = textArea.getSelectionEnd();
                        int delta = 0;
                        
                        int newLineIndex = text.lastIndexOf('\n', selectionStart - 1);
                        // The special not-found value, -1, can actually be handled with the same 
                        // logic as "normal" values.
                        undoRecorder.beginCompoundEdit();
                        do
                        {
                            // We need to use 'delta' when inserting, because the Document's copy 
                            // is going to be increasingly out-of-sync with 'text' as we insert 
                            // more into it.
                            doc.insertString(newLineIndex + delta + 1, "  ", null);
                            delta += INDENT.length();
                            newLineIndex = text.indexOf('\n', newLineIndex + 1);
                        }
                        while(newLineIndex < selectionEnd && newLineIndex != -1);
                        undoRecorder.endCompoundEdit();
                        
                        textArea.setSelectionStart(selectionStart + INDENT.length());
                        textArea.setSelectionEnd(selectionEnd + delta);
                    }
                    catch(BadLocationException e)
                    {
                        System.out.printf(e.getMessage());
                        logger.log(Level.SEVERE, "Out of bounds error while indenting", e);
                    }
                }
            }
        );
        
        actionMap.put(
            "Unindent",
            new AbstractAction("Unindent")
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    try
                    {
                        Document doc = textArea.getDocument();
                        String text = doc.getText(0, doc.getLength());
                        int selectionStart = textArea.getSelectionStart();
                        int selectionEnd = textArea.getSelectionEnd();
                        int delta = 0;
                        
                        int newLineIndex = text.lastIndexOf('\n', selectionStart - 1);
                        // The special not-found value, -1, can actually be handled with the same 
                        // logic as "normal" values.
                        
                        if(text.startsWith(INDENT, newLineIndex + 1))
                        {
                            selectionStart -= INDENT.length();
                        }
                        
                        undoRecorder.beginCompoundEdit();
                        do
                        {
                            // Check whether this line break actually has sufficient spaces
                            if(text.startsWith(INDENT, newLineIndex + 1))
                            {
                                // We need to use 'delta' when removing, because the Document's copy 
                                // is going to be increasingly out-of-sync with 'text' as we remove
                                // more from it.
                                doc.remove(newLineIndex + 1 - delta, INDENT.length());
                                delta += INDENT.length();
                            }
                            newLineIndex = text.indexOf('\n', newLineIndex + 1);
                        }
                        while(newLineIndex < selectionEnd && newLineIndex != -1);
                        undoRecorder.endCompoundEdit();
                        
                        textArea.setSelectionStart(selectionStart);
                        textArea.setSelectionEnd(selectionEnd - delta);
                    }
                    catch(BadLocationException e)
                    {
                        System.out.printf(e.getMessage());
                        logger.log(Level.SEVERE, "Out of bounds error while indenting", e);
                    }
                }
            }
        );
        
        InputMap inputMap = textArea.getInputMap();
        inputMap.put(KeyStroke.getKeyStroke("control Z"), "Undo");
        inputMap.put(KeyStroke.getKeyStroke("shift control Z"), "Redo");
        inputMap.put(KeyStroke.getKeyStroke("TAB"), "Indent");
        inputMap.put(KeyStroke.getKeyStroke("shift TAB"), "Unindent");
    }
    
    public void openFile()
    {
        fileChooser.setDialogTitle("Open Python Script");
        if(fileChooser.showOpenDialog(textArea) == JFileChooser.APPROVE_OPTION)
        {
            // Adapted from https://stackoverflow.com/a/5445161
            try
            {
                Scanner sc = new Scanner(fileChooser.getSelectedFile()).useDelimiter("\\A");
                String text = sc.next();
                
                IOException e = sc.ioException();
                if(e != null)
                {
                    throw e;
                }
                textArea.setText(text);
                textArea.setCaretPosition(0);
                textArea.requestFocusInWindow();
            }
            catch(IOException e)
            {
                JOptionPane.showMessageDialog(
                    textArea,
                    String.format(
                        "<html><p>Could not open script file \"<em>%s</em>\":</p><p>%s</p></html>", 
                        fileChooser.getSelectedFile().getName(), 
                        e.getMessage()),
                    "Script Load Error",
                    JOptionPane.ERROR_MESSAGE);
            }      
        }
    }
    
    public void saveFile()
    {
        fileChooser.setDialogTitle("Save Python Script");
        if(fileChooser.showSaveDialog(textArea) == JFileChooser.APPROVE_OPTION)
        {
            try(PrintWriter writer = new PrintWriter(fileChooser.getSelectedFile()))
            {
                String text = textArea.getText(); 
                writer.print(text);
                if(-1 == "\n\r".indexOf(text.charAt(text.length() - 1)))
                {
                    writer.println();
                }
            }
            catch(IOException e)
            {
                JOptionPane.showMessageDialog(
                    textArea,
                    String.format(
                        "<html><p>Could not save script to \"<em>%s</em>\":</p><p>%s</p></html>", 
                        fileChooser.getSelectedFile().getName(), 
                        e.getMessage()),
                    "Script Save Error",
                    JOptionPane.ERROR_MESSAGE);
            }      
        }
    }
    
    public void addTo(Container container)
    {
        container.add(scroller);
    }
    
    public JComponent getComponent()
    {
        return textArea;
    }
    
    public String getCode()
    {
        return textArea.getText();
    }
    
    private void setEditable(boolean editable)
    {
        textArea.setEditable(editable);
        textArea.getCaret().setVisible(editable && textArea.hasFocus());
    }
    
    @Override
    public void scenarioLoaded(final Scenario scenario)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() 
        {
            @Override
            public void run()
            {
                setEditable(true);
                textArea.setText(scenario.defaultCode());
                textArea.setCaretPosition(0);
                textArea.requestFocusInWindow();
                
                // Reset the undo manager here, so that it sees this text as the "original" state.
                undoer = new UndoManager();
            }
        });
    }
    
    @Override
    public void scenarioStarted(Scenario scenario)
    {
        setEditable(false);
    }
    
    @Override
    public void atLine(int lineN)
    {
        highlighter.removeAllHighlights();
        try
        {
            runHighlight = highlighter.addHighlight(
                textArea.getLineStartOffset(lineN), 
                textArea.getLineEndOffset(lineN), 
                runPainter);
        }
        catch(BadLocationException e)
        {
            // This seems unlikely, and even if it happens, we probably don't really want to do 
            // anything.
            logger.log(Level.WARNING, "Highlight failed", e);
        }
    }
    
    @Override
    public void errorAtLine(int lineN)
    {
        highlighter.removeAllHighlights();
        try
        {
            errorHighlight = highlighter.addHighlight(
                textArea.getLineStartOffset(lineN), 
                textArea.getLineEndOffset(lineN), 
                errorPainter);
        }
        catch(BadLocationException e)
        {
            // This seems unlikely, and even if it happens, we probably don't really want to do 
            // anything.
            logger.log(Level.WARNING, "Highlight failed", e);
        }
    }
    
    @Override
    public void scenarioFinished()
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() 
        {
            @Override 
            public void run() 
            {
                if(runHighlight != null)
                {
                    logger.finer("Removing highlighting");
                    highlighter.removeHighlight(runHighlight);
                    runHighlight = null;
                }
                setEditable(true);
                textArea.requestFocusInWindow();
            }            
        });
    }
}
