package edu.curtin.cs.codedemo.ui.display;
import edu.curtin.cs.codedemo.scenario.display.*;

import java.awt.Image;

/**
 * <p>Implementation of {@link DisplayFactory}. Each new display object is added to the 
 * {@link DisplayPabel}.</p>
 */
public class DisplayFactoryImpl implements DisplayFactory
{
    private DisplayPanel display;

    public DisplayFactoryImpl(DisplayPanel display)
    {
        this.display = display;
    }

    @Override
    public DisplayImage makeImage(Image image)
    {
        DisplayImageImpl di = new DisplayImageImpl(image);
        display.addObject(di);
        return di;
    }
    
    @Override
    public DisplayGrid makeGrid(int nRows, int nCols)
    {
        DisplayGridImpl dg = new DisplayGridImpl(nRows, nCols);
        display.addObject(dg);
        return dg;
    }
}
