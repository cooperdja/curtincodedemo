package edu.curtin.cs.codedemo.ui.display;
import edu.curtin.cs.codedemo.scenario.display.*;

import java.awt.Image;
import java.awt.Graphics2D;

public class DisplayImageImpl extends DisplayImage implements PaintableObject
{
    public DisplayImageImpl(Image image)
    {
        super(image);
    }
    
    @Override
    public void paint(Graphics2D gfx, DisplayPanel display, double squareSize)
    {
        int gridLineThickness = display.getLineThickness();
        int halfLineThickness = gridLineThickness / 2;

        AnimPoint position = position();
        AnimPoint size = size();
        double halfRowSize = size.y * 0.5;
        double halfColSize = size.x * 0.5;
        
        int pixelY = (int) (squareSize * (position.y - halfRowSize));
        int pixelHeight = (int) (squareSize * (position.y + halfRowSize)) - pixelY - gridLineThickness;
        pixelY += halfLineThickness;
        
        int pixelX = (int) (squareSize * (position.x - halfColSize));
        int pixelWidth = (int) (squareSize * (position.x + halfColSize)) - pixelX - gridLineThickness;
        pixelX += halfLineThickness;
        
        double rotateAngle = getAngle();
        if(!hasFlag(ROTATE_WITH_GRID))
        {
            rotateAngle -= display.getGridAngle();
        }
        
        gfx.rotate(rotateAngle, pixelX + pixelWidth / 2, pixelY + pixelHeight / 2);
        gfx.drawImage(getImage(), pixelX, pixelY, pixelWidth, pixelHeight, null);
    }
}
