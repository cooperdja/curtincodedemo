package edu.curtin.cs.codedemo.ui.display;

import java.awt.Graphics2D;

public interface PaintableObject
{
    int getZOrder();
    void paint(Graphics2D gfx, DisplayPanel display, double squareSize);
}
