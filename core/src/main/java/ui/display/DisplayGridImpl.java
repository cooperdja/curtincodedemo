package edu.curtin.cs.codedemo.ui.display;
import edu.curtin.cs.codedemo.scenario.display.*;

import java.awt.Color;
import java.awt.Graphics2D;

public class DisplayGridImpl extends DisplayGrid implements PaintableObject
{
    public DisplayGridImpl(int nRows, int nCols)
    {
        super(nRows, nCols);
    }
    
    @Override
    public void paint(Graphics2D gfx, DisplayPanel display, double squareSize)
    {
        int gridLineThickness = display.getLineThickness();
        int halfLineThickness = gridLineThickness / 2;
        
        int nRows = getNRows();
        int nCols = getNCols();
        AnimPoint scale = getScale();
        AnimPoint offset = getOffset();

        gfx.setColor(new Color(0, 0, 0, 0x80));
        gfx.fillRect(
            (int)(squareSize * offset.x), 
            (int)(squareSize * offset.y), 
            (int)(squareSize * ((double) nCols * scale.x)),
            (int)(squareSize * ((double) nRows * scale.y)));
    
        for(int row = 0; row <= nRows; row++)
        {
            double realY = squareSize * ((double) row * scale.y + offset.y);
            int y = (int) realY;
            int vertLength = (int) (realY + squareSize * scale.y) - y + gridLineThickness;
            y -= halfLineThickness;
            
            for(int col = 0; col <= nCols; col++)
            {
                double realX = squareSize * ((double) col * scale.x + offset.x);
                int x = (int) realX;
                int horizLength = (int) (realX + squareSize * scale.x) - x + gridLineThickness;
                x -= halfLineThickness;
                
                // Draw horizontal grid line (if any)
                Color clr = getHorizLine(row, col);
                if(clr != null)
                {
                    gfx.setColor(clr);
                    gfx.fillRect(x, y, horizLength, gridLineThickness);
                }
                
                // Draw vertical grid line (if any)
                clr = getVertLine(row, col);
                if(clr != null)
                {
                    gfx.setColor(clr);
                    gfx.fillRect(x, y, gridLineThickness, vertLength);
                }
                
            }
        }
    }
}
