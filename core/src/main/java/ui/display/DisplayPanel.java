package edu.curtin.cs.codedemo.ui.display;
import edu.curtin.cs.codedemo.ui.Resources;

import edu.curtin.cs.codedemo.controller.LoadObserver;
import edu.curtin.cs.codedemo.scenario.Scenario;
import edu.curtin.cs.codedemo.scenario.display.*;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.*;
import javax.imageio.*;
import javax.swing.*;



/*
TODO:
- replace square colours with a simple DisplayRect subclass of DisplayObject
- introduce layers, identified by non-negative integers, each containing its own list of DisplayObjects. DisplayPanel should paint layers from lowest to highest.
*/


public class DisplayPanel extends Display implements LoadObserver
{
    private static final int DEFAULT_WIDTH = 640;
    private static final int DEFAULT_HEIGHT = 480;
    
    private JPanel panel;

    private List<PaintableObject> objects = new ArrayList<>();
    private boolean reorderObjects = true;

    private Comparator<PaintableObject> zOrderComparator = new Comparator<PaintableObject>()
    {
        @Override
        public int compare(PaintableObject o1, PaintableObject o2)
        {
            return o1.getZOrder() - o2.getZOrder();
        }
    };

    public DisplayPanel(final Resources resources)
    {
        panel = new JPanel() 
        {
            {
                setOpaque(false);
                setBackground(new Color(0, 0, 0, 0x80));
            }
            
            private ImageIcon bgImage = resources.getIcon("curtin-000.png");
            
            @Override
            public Dimension getPreferredSize() 
            {
                return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
            }        
            
            @Override
            public void paintComponent(Graphics g) 
            {
                super.paintComponent(g);  
                
                int imageWidth = bgImage.getIconWidth();
                int imageHeight = bgImage.getIconHeight();
                double imageAspect = (double)imageWidth / (double)imageHeight;                
                
                int taWidth = getWidth();
                int taHeight = getHeight();
                double taAspect = (double)taWidth / (double)taHeight;
                
                if(imageAspect > taAspect)
                {
                    // Image is more short-and-wide than the text area
                    // -> Scale it to fit vertically, and crop horizontally
                    
                    int scaledWidth = (int)Math.round((double)taHeight * imageAspect);
                    
                    g.drawImage(bgImage.getImage(), 
                        (taWidth - scaledWidth) / 2, 0, 
                        scaledWidth, taHeight,
                        Color.BLACK, null);
                }
                else
                {
                    // Image is more tall-and-narrow than the text area
                    // -> Scale it to fit horizontally, and crop vertically
                    
                    int scaledHeight = (int)Math.round((double)taWidth / imageAspect);
                    g.drawImage(bgImage.getImage(), 
                        0, (taHeight - scaledHeight) / 2,
                        taWidth, scaledHeight,
                        Color.BLACK, null);
                }
                
                DisplayPanel.this.paint((Graphics2D) g);
            }
        };
        panel.setBackground(getBgColour());
    }
    
    public void addTo(Container container)
    {
        container.add(panel);
    }
    
    @Override
    public void reset()
    {
        super.reset();
        objects.clear();
        reorderObjects = true;
        panel.repaint();
    }
    
    @Override
    public void reorder()
    {
        reorderObjects = true;
    }
    
    @Override
    public void setBgColour(Color colour)
    {
        super.setBgColour(colour);
        panel.setBackground(colour);
    }
    
    public void addObject(PaintableObject object) 
    {
        objects.add(object);
        reorderObjects = true;
    }

    @Override
    public void update()
    {
        panel.repaint();
    }
    
    @Override
    public void scenarioLoaded(Scenario scenario)
    {
        panel.repaint();
    }

    private void paint(Graphics2D gfx)
    {
        int gridLineThickness = getLineThickness();
        int halfLineThickness = gridLineThickness / 2;
        double spaceWidth = (double) panel.getWidth();
        double spaceHeight = (double) panel.getHeight();
        
        double gridAngle = getGridAngle();
        
        AnimPoint size = size();
        AnimPoint offset = offset();
        
        // Rotate the grid by whatever amount is current, and keep track of the transformation object.
        gfx.rotate(gridAngle, spaceWidth / 2.0, spaceHeight / 2.0);
        
        /* Animation appears choppy without bicubic interpolation (for reasons I don't understand).
        Setting antialiasing to ON also appears to solve the issue, but I suspect this might just
        set bicubic interpolation indirectly. */
        gfx.setRenderingHint(RenderingHints.KEY_INTERPOLATION, 
                             RenderingHints.VALUE_INTERPOLATION_BICUBIC);
//         gfx.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        /* Work out the dimension of a single grid square, such that the grid will fit horizontally and vertically.
        - squareSize1 is our calculation assuming angles of 0, 180, 360, etc degrees, when columns are arranged horizontally and rows vertically.
        - squareSize2 is our calculation for angles of 90, 270, etc. degrees.
        - squareSize uses an occillating function to work out which to use, or where in between, for angles not evenly divisible by 90 degrees. This function *does not* guarantee that the slanted grids will actually fit. In fact, they won't, as the corners will get cut off. However, for animation purposes slanted grids will appear to be sized consistently with the strictly horizontal or vertical grids. */
        double squareSize1 = Math.min(
            (spaceWidth - gridLineThickness) / size.x,
            (spaceHeight - gridLineThickness) / size.y);
            
        double squareSize2 = Math.min(
            (spaceWidth - gridLineThickness) / size.y,
            (spaceHeight - gridLineThickness) / size.x);
            
        double squareSize = squareSize2 + (squareSize1 - squareSize2) * 0.5 * (Math.cos(gridAngle * 2.0) + 1.0);
        
        // If the space given is not exactly the same shape as the grid, some space will be left over. Center the grid in that space.
        gfx.translate(
            (spaceWidth - squareSize * size.x) / 2.0 - squareSize * offset.x, 
            (spaceHeight - squareSize * size.y) / 2.0 - squareSize * offset.y
        );
        AffineTransform baseTransform = gfx.getTransform();   

        if(reorderObjects)
        {
            objects.sort(zOrderComparator);
            reorderObjects = false;
        }
        
        // Draw objects (if any)
        for(PaintableObject obj : objects)
        {
            gfx.setTransform(baseTransform);
            obj.paint(gfx, this, squareSize);
        }
    }  
}
