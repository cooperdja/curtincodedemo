package edu.curtin.cs.codedemo;

import edu.curtin.cs.codedemo.scenario.*;
import edu.curtin.cs.codedemo.scenario.display.*;
import edu.curtin.cs.codedemo.ui.*;
import edu.curtin.cs.codedemo.ui.display.*;
import edu.curtin.cs.codedemo.controller.*;

import java.io.File;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class CodeDemo
{
    public static void main(final String[] args)
    {
        if(args.length > 1)
        {
            System.err.println("Maximum of one command-line parameter (the name of the scenario to load)");
        }
        else
        {
            try
            {
                // Attempt to set Swing look-and-feel:
                UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            }
            catch(ReflectiveOperationException | UnsupportedLookAndFeelException e)
            {
                // Oh well. Fall back to the default.
            }
        
            javax.swing.SwingUtilities.invokeLater
            (
                new Runnable() 
                {
                    public void run() 
                    {
                        Resources resources = new Resources();
                        DisplayPanel display = new DisplayPanel(resources);
                        CodeEditor editor = new CodeEditor(resources);
                        ScenarioToolbar toolbar = new ScenarioToolbar();
                        DisplayFactory factory = new DisplayFactoryImpl(display);
                        
                        HelpWindow helpWin = new HelpWindow(resources);
                        MainWindow win = new MainWindow(display, editor, toolbar, helpWin, resources);
                        
                        ScenarioLoader loader = new ScenarioLoader();
                        ScenarioRunner runner = new ScenarioRunner();
                        ControllerImpl controller = new ControllerImpl(
                            loader, runner, display, editor, factory);
                        
                        ScenarioPickerWindow scenarioPicker = new ScenarioPickerWindow(win, loader.loadAll());
                        win.setScenarioPicker(scenarioPicker);

                        win.setController(controller);
                        helpWin.setController(controller);
                        
                        runner.addStartObserver(editor);
                        runner.addRunObserver(editor);
                        runner.addFinishObserver(editor);
                        runner.addFinishObserver(win);
                        
                        controller.addLoadObserver(editor);
                        controller.addLoadObserver(display);
                        controller.addLoadObserver(toolbar);
                        
                        win.setVisible(true);
                        
                        if(args.length == 1)
                        {
                            win.loadScenario(new File(args[0]));
                        }
                        else
                        {
                            scenarioPicker.setVisible(true);
                        }
                    }
                }
            );
        }
    }
}
