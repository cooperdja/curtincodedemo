package edu.curtin.cs.codedemo.scenario;
import edu.curtin.cs.codedemo.scenario.display.*;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.*;

public abstract class Scenario
{
    private File file = null;
    private ScenarioResources resources = null;
    private Display display = null;
    private DisplayFactory factory = null;
    
    private String name = getClass().getName();
    private String author = null;
    private String iconFilename = null;
    private String helpFilename = null;

    public Scenario() 
    {
        ScenarioDetails details = getClass().getAnnotation(ScenarioDetails.class);
        if(details != null)
        {
            name = details.name();
            author = details.author();
            iconFilename = details.iconFile();
            helpFilename = details.helpFile();
        }
    }
    
    public void setFile(File file)
    {
        this.file = file;
    }
    
    public void setResources(ScenarioResources resources)
    {
        this.resources = resources;
    }
    
    public void setDisplay(Display display)
    {
        this.display = display;
    }
    
    public void setFactory(DisplayFactory factory)
    {
        this.factory = factory;
    }
        
    public String getName()                 { return name; }
    public String getAuthor()               { return author; }
    public String getIconFilename()         { return iconFilename; }
    public String getHelpFilename()         { return helpFilename; }
    public File getFile()                   { return file; }
    public ScenarioResources getResources() { return resources; }
    public Display getDisplay()             { return display; }
    
    public abstract String defaultCode();
    public abstract void setup() throws ScenarioSetupException, IOException;
    public abstract void reset() throws InterruptedException;
    
    public Animator makeAnimator(double duration, double frameRate)
    {
        return new Animator(display, duration, frameRate);
    }
    
    public DisplayImage makeImage(Image image)
    {
        return factory.makeImage(image);
    }
    
    public DisplayImage makeImage(String filename) throws ScenarioSetupException
    {
        try
        {
            return factory.makeImage(resources.getImage(filename));
        }
        catch(IOException e)
        {
            throw new ScenarioSetupException("Could not load image '" + filename + "'", e);
        }
    }
    
    public DisplayGrid makeGrid(IntPoint p)
    {
        return factory.makeGrid(p.getRow(), p.getCol());
    }
    
    public DisplayGrid makeGrid(int nRows, int nCols)
    {
        return factory.makeGrid(nRows, nCols);
    }
}
