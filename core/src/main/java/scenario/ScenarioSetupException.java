package edu.curtin.cs.codedemo.scenario;
import edu.curtin.cs.codedemo.controller.ScenarioLoadException;

public class ScenarioSetupException extends ScenarioLoadException
{
    public ScenarioSetupException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
    
    public ScenarioSetupException(String msg)
    {
        super(msg);
    }
}
