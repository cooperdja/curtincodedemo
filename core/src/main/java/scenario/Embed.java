package edu.curtin.cs.codedemo.scenario;

import java.lang.annotation.*;

/**
 * <p>Indicates that a method or field should be embedded in the interpreter, and thus made 
 * directly available to the user of the scenario. {@literal @Embed} methods are the principal
 * means by which the user accesses scenario-specific functionality.</p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface Embed
{    
}
