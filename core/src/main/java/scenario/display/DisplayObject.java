package edu.curtin.cs.codedemo.scenario.display;

/**
 * <p>Common superclass for display objects. At present, the only common feature among display 
 * objects is their z-orderability.</p>
 */
public class DisplayObject<D extends DisplayObject<D>>
{
    private int zOrder = 0;
        
    @SuppressWarnings("unchecked")
    public D zOrder(int zOrder)
    {
        this.zOrder = zOrder;
        return (D)this;
    }

    public int getZOrder()
    {
        return zOrder;
    }
}
