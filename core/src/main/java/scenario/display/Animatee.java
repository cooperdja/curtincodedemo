package edu.curtin.cs.codedemo.scenario.display;

/**
 * <p>Represents an object that can be animated by {@link Animator}.</p>
 */
public interface Animatee
{
    /**
     * <p>Causes the animatee to update itself in accordance with the specified level of progress
     * through the animation.</p>
     *
     * @param progress <p>The animation progress, in the range 0-1. Zero indicates the start of the 
     * animation (where the animatee's state should be as it originally was). One indicates the 
     * end, where the animatee should be in its final state (for this animation).</p>
     */
    void setAnimProgress(double progress);
}
