package edu.curtin.cs.codedemo.scenario.display;

import java.awt.Image;
import java.awt.Graphics2D;

/**
 * <p>A display object based on an image; a sprite.</p>
 */
public class DisplayImage extends DisplayObject<DisplayImage>
{
    public static final int ROTATE_WITH_GRID = 1;

    private Image image;
    private AnimPoint position;
    private AnimPoint size;
    private double angle;

    private int flags;
    
    public DisplayImage(Image image)
    {
        this.image = image;
        this.position = new AnimPoint(0.0, 0.0);
        this.size = new AnimPoint(1.0, 1.0);
    }
    
    public DisplayImage size(double width, double height)
    {
        size.x = width;
        size.y = height;
        return this;
    }
    
    public DisplayImage size(AnimPoint p)
    {
        return size(p.x, p.y);
    }
    
    public DisplayImage size(IntPoint p)
    {
        return size((double)p.getCol(), (double)p.getRow());
    }
    
    public DisplayImage at(double x, double y)
    {
        position.x = x;
        position.y = y;
        return this;
    }
    
    public DisplayImage at(AnimPoint p)
    {
        return at(p.x, p.y);
    }
    
    public DisplayImage at(IntPoint p)
    {
        return at((double)p.getCol(), (double)p.getRow());
    }
    
    public DisplayImage angle(double angle)
    {
        this.angle = angle;
        return this;
    }
    
    public DisplayImage flags(int flags)
    {
        this.flags = flags;
        return this;
    }
    
    public AnimPoint size()
    {
        return size;
    }
    
    public AnimPoint position()
    {
        return position;
    }
    
    public double getAngle()
    {
        return angle;
    }
    
    public Image getImage()
    {
        return image;
    }
         
    public boolean hasFlag(int flag)
    {
        return (flags & flag) == flag;
    }
}
