package edu.curtin.cs.codedemo.scenario.display;

/**
 * <p>A version of MultiAnimation that skews the animation progress so that it accelerates and then 
 * decelerates. The acceleration is constant, and then the deceleration is a constant of the 
 * opposite sign. Effectively, therefore, the properties change as an S-shaped function, made by 
 * joining two quadratic equations end-to-end.</p>
 */
public class QuadraticAnimation extends MultiAnimation
{
    /**
     * <p>Constructs a QuadraticAnimation given a list of zero or more child animatees.</p>
     */
    public QuadraticAnimation(Animatee... children)
    {
        super(children);
    }
    
    /**
     * <p>Constructs a QuadraticAnimation by copying the children of an existing MultiAnimation 
     * (which may or may not be another QuadraticAnimation).</p>
     */
    public QuadraticAnimation(MultiAnimation animationList)
    {
        super(animationList);
    }
    
    /**
     * <p>Maps a linear progress value onto a quadratic one.</p>
     *
     * @param progress The original progress value (0-1).
     * @return A modified progress value (0-1).
     */
    @Override
    public double skewProgress(double progress)
    {
        if(progress < 0.5)
        {
            // Accelerating
            return 2.0 * progress * progress;
        }
        else 
        {
            // Decelerating                            
            return progress * (4.0 - 2.0 * progress) - 1.0;
        }
    }
}
