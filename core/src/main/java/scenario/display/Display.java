package edu.curtin.cs.codedemo.scenario.display;

import java.awt.*;
import java.util.*;


/*
TODO:
- replace square colours with a simple DisplayRect subclass of DisplayObject
*/

/** 
 * <p>Represents the overall display area that the scenario has access to.</p>
 */
public abstract class Display
{
    public static final Color DEFAULT_BACKGROUND = Color.WHITE;
    public static final int DEFAULT_LINE_THICKNESS = 2;
    public static final double DEFAULT_GRID_ANGLE = 0.0;    
    
    private AnimPoint gridOffset;
    private AnimPoint gridSize;
    private int gridLineThickness;
    private double gridAngle;    
    private Color bgColour;

    public Display()
    {
        gridOffset = new AnimPoint(0.0, 0.0);
        gridSize = new AnimPoint(1.0, 1.0);
        gridLineThickness = DEFAULT_LINE_THICKNESS;
        gridAngle = DEFAULT_GRID_ANGLE;
        bgColour = DEFAULT_BACKGROUND;
    }
    
    /**
     * <p>Sets the display area back to its default (initial) state.</p>
     */
    public void reset()
    {
        gridOffset.set(0.0, 0.0);
        gridSize.set(1.0, 1.0);
        gridLineThickness = DEFAULT_LINE_THICKNESS;
        gridAngle = DEFAULT_GRID_ANGLE;
        bgColour = DEFAULT_BACKGROUND;
    }
    
    /** 
     * <p>Causes the display area to be physically re-drawn on the screen. This is done as part of
     * the animation process, but may need to be called directly if the scenario performs other 
     * kinds of updates.</p>     
     */
    public abstract void update();
    
    /**
     * <p>Re-evaluates the z-ordering of objects displayed. It is only necessary to call this 
     * method if the scenario alters the z-ordering of existing display objects.</p>
     */
    public abstract void reorder();
    
    /**
     * <p>Returns and exposes the top-left corner of the target display area.</p>
     */
    public AnimPoint offset()
    {
        return gridOffset;
    }
    
    /**
     * <p>Returns and exposes the size of the target display area.</p>
     */
    public AnimPoint size()
    {
        return gridSize;
    }
    
    public void setGridAngle(double gridAngle)
    {
        this.gridAngle = gridAngle;
    }
    
    public double getGridAngle()
    {
        return gridAngle;
    }
    
    public void setLineThickness(int thickness)
    {
        gridLineThickness = thickness;
    }
    
    public void setBgColour(Color bgColour)
    {
        this.bgColour = bgColour;
    }
    
    public Color getBgColour()
    {
        return bgColour;
    }
    
    public int getLineThickness()
    {
        return gridLineThickness;
    }
}
