package edu.curtin.cs.codedemo.scenario.display;

import java.awt.geom.Point2D;

/**
 * <p>Represents an animatable, real-valued 2D point.</p>
 */
public class AnimPoint extends Point2D.Double implements Animatee
{
    private double fromX, fromY, deltaX, deltaY;
    
    public AnimPoint(double x, double y)
    {
        super(x, y);
    }
    
    public AnimPoint(IntPoint p)
    {
        super((double)p.getCol(), (double)p.getRow());
    }
    
    /**
     * <p>Multiplies the X and Y coordinates, in-place, by a scalar factor.</p>
     *
     * @param by The multiplicative factor.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint scale(double by)
    {
        this.x *= by;
        this.y *= by;
        return this;
    }
    
    /**
     * <p>On the next animation, move this point by the specified amount (relative to its current 
     * value).</p>
     *
     * @param deltaX The distance to move in the X direction.
     * @param deltaY The distance to move in the Y direction.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint moveBy(double deltaX, double deltaY)
    {
        fromX = this.x;
        fromY = this.y;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        return this;
    }
    
    /**
     * <p>On the next animation, move this point by the specified amount (relative to its current 
     * value).</p>
     *
     * @param delta A {@link Point2D} object (which could be another AnimPoint), representing a
     * relative distance, in each of the X and Y directions, to move.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint moveBy(Point2D delta)
    {
        return moveBy(delta.getX(), delta.getY());
    }
    
    /**
     * <p>On the next animation, move this point by the specified integer amount (relative to its 
     * current value). This is a convenience method taking in a {@link IntPoint} object. Note that
     * the current point is moved <em>by</em> an integer amount, not <em>to</em> an integer 
     * position (unless it was already at one).</p>
     *
     * @param delta A relative integer distance to move, in each of the X and Y directions.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint moveBy(IntPoint delta)
    {
        return moveBy((double)delta.getCol(), (double)delta.getRow());
    }
    
    /**
     * <p>On the next animation, move this point to the specified absolute location.</p>
     *
     * @param toX The final horizontal position.
     * @param toY The final vertical position.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint moveTo(double toX, double toY)
    {
        fromX = this.x;
        fromY = this.y;
        deltaX = toX - fromX;
        deltaY = toY - fromY;
        return this;
    }
    
    /**
     * <p>On the next animation, move this point to the specified absolute location.</p>
     *
     * @param to A {@link Point2D} object (which could be another AnimPoint), representing the 
     * final position to move to.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint moveTo(Point2D to)
    {
        return moveTo(to.getX(), to.getY());
    }
    
    /**
     * <p>On the next animation, move this point to the specified absolute integer location. This
     * is a convenience method taking in a {@link IntPoint} object.</p>
     *
     * @param to An {@link IntPoint} object, representing the final integer position to move to.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint moveTo(IntPoint to)
    {
        return moveTo((double)to.getCol(), (double)to.getRow());
    }

    /**
     * <p>Set the X and Y coordinates of this point.</p>
     * 
     * @param x The new X coordinate.
     * @param y The new Y coordinate.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint set(double x, double y)
    {
        this.x = x;
        this.y = y;
        return this;
    }
    
    /**
     * <p>Set the X and Y coordinates of this point, by copying those of an existing 
     * {@link Point2D} object (which could be another AnimPoint).</p>
     * 
     * @param p The point to copy.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint set(Point2D p)
    {
        return set(p.getX(), p.getY());
    }
    
    /**
     * <p>Set the X and Y coordinates of this point, by copying those of an existing 
     * {@link IntPoint} object.</p>
     * 
     * @param p The point to copy.
     * @return This AnimPoint object, allowing for method chaining.
     */
    public AnimPoint set(IntPoint p)
    {
        return set(p.getCol(), p.getRow());
    }
    
    /**
     * <p>Sets the point to a position somewhere between the original and the final, according to
     * the given level of animation progress.</p>
     *
     * @param progress The animation progress, in the range 0-1.
     */
    @Override
    public synchronized void setAnimProgress(double progress)
    {
        this.x = fromX + deltaX * progress;
        this.y = fromY + deltaY * progress;
    }
}
