package edu.curtin.cs.codedemo.scenario.display;

import java.awt.Color;

/**
 * <p>A display object representing a series of horizontal and vertical grid lines, each of which
 * can be turned on or off independently.</p>
 */
public class DisplayGrid extends DisplayObject<DisplayGrid>
{
    private Color[][] vertLines;
    private Color[][] horizLines;
    
    private int nRows, nCols;
    private AnimPoint offset;
    private AnimPoint scale;
    
    public DisplayGrid(IntPoint p)
    {
        this(p.getRow(), p.getCol());
    }
    
    public DisplayGrid(int nRows, int nCols)
    {
        this.nRows = nRows;
        this.nCols = nCols;
        offset = new AnimPoint(0.0, 0.0);
        scale = new AnimPoint(1.0, 1.0);
//         vertLines = new Color[nRows][nCols + 1];
//         horizLines = new Color[nRows + 1][nCols];
        vertLines = new Color[nRows + 1][nCols + 1];
        horizLines = new Color[nRows + 1][nCols + 1];
    }
        
    public int getNRows()
    {
        return nRows;
    }
    
    public int getNCols()
    {
        return nCols;
    }
    
    public AnimPoint getOffset()
    {
        return offset;
    }
    
    public AnimPoint getScale()
    {
        return scale;
    }
    
    public Color getHorizLine(int row, int col)
    {
        return horizLines[row][col];
    }
    
    public Color getVertLine(int row, int col)
    {
        return vertLines[row][col];
    }
    
    public void setOffset(double x, double y)
    {
        offset.x = x;
        offset.y = y;
    }
    
    public void setScale(double x, double y)
    {
        scale.x = x;
        scale.y = y;
    }
    
    public void setHorizLines(IntPoint start, IntPoint end, Color colour)
    {
        setLines(horizLines, start.getRow(), start.getCol(), end.getRow(), end.getCol(), colour);
    }
        
    public void setHorizLines(int startRowN, int startColN, int endRowN, int endColN, Color colour)
    {
        setLines(horizLines, startRowN, startColN, endRowN, endColN, colour);
    }
    
    public void setVertLines(IntPoint start, IntPoint end, Color colour)
    {
        setLines(vertLines, start.getRow(), start.getCol(), end.getRow(), end.getCol(), colour);
    }
    
    public void setVertLines(int startRowN, int startColN, int endRowN, int endColN, Color colour)
    {
        setLines(vertLines, startRowN, startColN, endRowN, endColN, colour);
    }
    
    private void setLines(Color[][] gridLineSet, int startRowN, int startColN, int endRowN, int endColN, Color colour)
    {
        for(int row = startRowN; row <= endRowN; row++)
        {
            for(int col = startColN; col <= endColN; col++)
            {
                gridLineSet[row][col] = colour;
            }
        }
    }
}
