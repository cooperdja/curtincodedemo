package edu.curtin.cs.codedemo.scenario.display;

import java.awt.Image;

/**
 * <p>Interface for creating new display objects.</p>
 */
public interface DisplayFactory
{
    DisplayImage makeImage(Image image);
    DisplayGrid makeGrid(int nRows, int nCols);    
}
