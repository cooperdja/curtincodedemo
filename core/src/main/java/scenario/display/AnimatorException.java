package edu.curtin.cs.codedemo.scenario.display;

public class AnimatorException extends RuntimeException
{
    public AnimatorException(String msg) 
    { 
        super(msg); 
    }
}
