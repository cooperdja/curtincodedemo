package edu.curtin.cs.codedemo.scenario.display;

/** 
 * <p>An immutable, integer-based 2D point.</p>
 */
public class IntPoint 
{
    public static IntPoint ORIGIN = new IntPoint(0, 0);

    private final int row;
    private final int col;
    
    public static IntPoint xy(int x, int y)
    {
        return new IntPoint(y, x);
    }
    
    public static IntPoint rc(int row, int col)
    {
        return new IntPoint(row, col);
    }

    public IntPoint(int row, int col)
    {
        this.row = row;
        this.col = col;
    }
    
    public int getRow() { return row; }
    public int getCol() { return col; }
    public int getX()   { return col; }
    public int getY()   { return row; }
    
    /** 
     * Perform a 2D boolean array lookup based on this point. That is, use the row and column 
     * within this object to index a 2D array. This is equivalent to:
     * <code>array[pos.getRow()][pos.getCol()]</code>.
     */
    public boolean index(boolean[][] array)
    {
        return array[row][col];
    }
    
    /** Perform a 2D int array lookup based on this point. */
    public int index(int[][] array)
    {
        return array[row][col];
    }
    
    /** Perform a 2D byte array lookup based on this point. */
    public byte index(byte[][] array)
    {
        return array[row][col];
    }
        
    public IntPoint plus(IntPoint p)
    {
        return new IntPoint(row + p.row, col + p.col);
    }
    
    /** Return a new IntPoint, one up from this point. */
    public IntPoint up()
    {
        return new IntPoint(row - 1, col);
    }
    
    /** Return a new IntPoint, one down from this point. */
    public IntPoint down()
    {
        return new IntPoint(row + 1, col);
    }
    
    /** Return a new IntPoint, one to the left of this point. */
    public IntPoint left()
    {   
        return new IntPoint(row, col - 1);
    }

    /** Return a new IntPoint, one to the right of this point. */
    public IntPoint right()
    {
        return new IntPoint(row, col + 1);
    }
    
    /** Return true iff this point is higher than another. (Column values are ignored.) */
    public boolean above(IntPoint other)
    {
        return row < other.row;
    }
    
    /** Return true iff this point is lower than another. (Column values are ignored.) */
    public boolean below(IntPoint other)
    {
        return row > other.row;
    }
    
    /** Return true iff this point is to the left of another. (Row values are ignored.) */
    public boolean leftOf(IntPoint other)
    {
        return col < other.col;
    }
    
    /** Return true iff this point is to the right of another. (Row values are ignored.) */
    public boolean rightOf(IntPoint other)
    {
        return col > other.col;
    }
    
    
    @Override 
    public boolean equals(Object obj)
    {
        boolean eq = false;
        if(obj instanceof IntPoint)
        {
            IntPoint p = (IntPoint)obj;
            eq = (row == p.row) && (col == p.col);
        }
        return eq;
    }
    
    @Override
    public int hashCode()
    {
        return 37 * row + col;
    }
    
    @Override
    public String toString()
    {
        return String.format("(r%d,c%d)", row, col);
    }
}
