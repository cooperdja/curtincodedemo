package edu.curtin.cs.codedemo.scenario.display;

import java.util.*;

/**
 * <p>Conducts an animation sequence, lasting for a certain period of time, at a certain frame 
 * rate, and involving a certain number of {@link Animatee} objects.</p>
 */
public class Animator
{
    private Display display;
    private MultiAnimation animateeList = new MultiAnimation();

    private int delay;
    private int nFrames;
    private int frameN = 1;
    
    private Timer timer;
    private Object animationMonitor = new Object();
    
    
    
    
    public Animator(Display display, double duration, double frameRate)
    {
        this.display = display;
        this.nFrames = (int) (frameRate * duration);
        this.delay = (int) (1000.0 / frameRate); // Delay between frames in milliseconds
    }

    /**
     * <p>Specify one or more {@link Animatee}s to be part of this animation. Each Animatee is
     * assumed to already know what its final state is; i.e. what is going to happen to it during 
     * the animation. (To specify this, you must consult the documentation for the relevant 
     * Animatee subclass.)</p>
     *
     * @param animatees One or more Animatee objects to animate.
     * @return This Animator (to help operation chaining).
     */
    public Animator of(Animatee... animatees)
    {
        animateeList.of(animatees);
        return this;
    }
    
    /**
     * <p>Specify that the animation should be quadratic rather than linear. Quadratic animations
     * give a feel of acceleration and deceleration, such that the animation starts and ends 
     * slowly, and proceeds rapidly in the middle. (Linear animations, by contrast, happen at a 
     * constant speed.)</p>
     *
     * @return This Animator (to help operation chaining).
     */
    public Animator quadratic()
    {
        animateeList = new QuadraticAnimation(animateeList);
        return this;
    }
    
    
    /**
     * <p>Carries out the animation.</p>
     *
     * @throws InterruptedException If the animation is interrupted part-way through.
     */
    public void animate() throws InterruptedException
    {
        if(timer != null)
        {
            throw new AnimatorException(
                "animate() should not be called more than once on a given Animator object.");
        }
        
        synchronized(animationMonitor)
        {
            TimerTask animationTask = new TimerTask()
            {
                @Override
                public void run()
                {
                    // Calculate the progress through the animation (0-1)
                    double progress = (double) frameN / (double) nFrames;
                    
                    if(progress <= 1.0)
                    {
                        animateeList.setAnimProgress(progress);
                        frameN++;
                    }
                    else
                    {
                        // End of animation.
                        synchronized(animationMonitor)
                        {
                            // If we weren't accurate enough during the animation to arrive at
                            // the exact desired final state, we could make a final correction
                            // here. But so far this doesn't appear to be necessary.
                            
                            animationMonitor.notify();
                        }
                    }
                    display.update(); // Schedule a GUI update.
                }
            };

            timer = new Timer("animation");
            timer.scheduleAtFixedRate(animationTask, delay, delay);

            // Block until the animation is complete
            try
            {
                animationMonitor.wait();
            }
            catch(InterruptedException e) 
            { 
                if(frameN <= nFrames)
                {
                    // Animation actually interrupted.
                    throw e;
                }
                else {} // Animation finished properly.
            }
            finally
            {
//                 timer.stop();
                timer.cancel();
            }
        }
    }    
}

