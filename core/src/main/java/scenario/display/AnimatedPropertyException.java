package edu.curtin.cs.codedemo.scenario.display;

class AnimatedPropertyException extends RuntimeException
{
    public AnimatedPropertyException(String msg)                  { super(msg); }
    public AnimatedPropertyException(Throwable cause)             { super(cause); }    
    public AnimatedPropertyException(String msg, Throwable cause) { super(msg, cause); }
}
