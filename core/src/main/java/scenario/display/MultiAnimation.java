package edu.curtin.cs.codedemo.scenario.display;

import java.util.*;

/**
 * <p>A composite {@link Animatee} that bundles together a group of Animatees, causing them all
 * to be animated simultaneously.</p>
 */
public class MultiAnimation implements Animatee
{
    private List<Animatee> children = new ArrayList<>();

    /**
     * <p>Constructs a MultiAnimation given a list of zero or more child animatees.</p>
     */
    public MultiAnimation(Animatee... children)
    {
        for(Animatee child : children)
        {   
            this.children.add(child);
        }
    }
    
    /**
     * <p>Constructs a MultiAnimation by copying the children of an existing MultiAnimation.</p>
     */
    public MultiAnimation(MultiAnimation animationList)
    {
        children.addAll(animationList.children);
    }
    
    /**
     * <p>Adds one or more child animatees to the bundle.</p>
     */
    public MultiAnimation of(Animatee... children)
    {
        for(Animatee child : children)
        {
            this.children.add(child);
        }
        return this;
    }
    
    /**
     * <p>Updates the child animatees, in accordance with the specified level of progress through 
     * the animation.</p>
     *
     * @param progress The animation progress, in the range 0-1.
     */
    @Override
    public synchronized void setAnimProgress(double progress)
    {
        progress = skewProgress(progress);
        for(Animatee child : children)
        {
            child.setAnimProgress(progress);
        }
    }
    
    /**
     * <p>Performs a transformation on the animation progress, to achieve various speed and timing
     * effects. The default implementation makes no alterations (it is the identity function), but 
     * the intention is for subclasses to override it.</p>
     *
     * @param progress The original progress value (0-1).
     * @return A modified progress value (0-1).
     */
    public double skewProgress(double progress)
    {
        return progress;
    }
}
