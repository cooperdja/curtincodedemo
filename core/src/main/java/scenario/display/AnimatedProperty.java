package edu.curtin.cs.codedemo.scenario.display;

import java.lang.reflect.Method;

/**
 * <p>Represents a double-valued "property" to be provided to {@link Animator}, so that it can 
 * participate in an animation.</p>
 *
 * <p>A property is defined as the combination of a getter and setter on a particular object.
 * The getter must be of the form '{@code public double getXyz()}', and the setter 
 * '{@code public void setXyz(double val)}', where '{@code Xyz}' is the same in both cases.</p>
 *
 * <p>This class allows animation of <em>any</em> double value on any object, without the need for
 * for any adapter code, provided the class adheres to the above convention.</p>
 */
public class AnimatedProperty implements Animatee
{
    private Object obj;
    private Method getter, setter;
    private double from, delta;
    
    public AnimatedProperty(Object obj, String propertyName) throws AnimatedPropertyException
    {
        this.obj = obj;
    
        // Validate the property name
        for(char ch : propertyName.toCharArray())
        {
            if(!Character.isJavaIdentifierPart(ch))
            {
                throw new AnimatedPropertyException("'%s' is not a valid identifier".format(propertyName));
            }
        }
        
        // Capitalise first letter
        propertyName = Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
        
        try
        {
            Class<?> cls = obj.getClass();
            getter = cls.getMethod("get" + propertyName);
            setter = cls.getMethod("set" + propertyName, double.class);
        }
        catch(NoSuchMethodException e)
        {
            throw new AnimatedPropertyException(e);
        }
        
        if(getter.getReturnType() != double.class)
        {
            throw new AnimatedPropertyException("'get%s' should return 'double', not '%s'".format(propertyName, getter.getReturnType().getName()));
        }
    }
    
    private void animateFrom() throws AnimatedPropertyException
    {
        try
        {
            this.from = (Double)getter.invoke(obj);
        }
        catch(ReflectiveOperationException e)
        {
            throw new AnimatedPropertyException(e);
        }
    }
    
    /**
     * <p>On the next animation, change this property to the specified value.</p>
     *
     * @param to The new final value.
     * @return This AnimatedProperty object, allowing for method chaining.
     */
    public AnimatedProperty moveTo(double to) throws AnimatedPropertyException
    {
        animateFrom();
        this.delta = to - from;
        return this;
    }
    
    /**
     * <p>On the next animation, change this property by the specified amount (relative to its 
     * current value).</p>
     *
     * @param delta The distance to move.
     * @return This AnimatedProperty object, allowing for method chaining.
     */
    public AnimatedProperty moveBy(double delta) throws AnimatedPropertyException
    {
        animateFrom();
        this.delta = delta;
        return this;
    }
    
    /**
     * <p>Sets the property to a value somewhere between the original and the final values, 
     * according to the given level of animation progress.</p>
     *
     * @param progress The animation progress, in the range 0-1.
     */
    @Override
    public synchronized void setAnimProgress(double progress) throws AnimatedPropertyException
    {
        try
        {
            setter.invoke(obj, from + delta * progress);
        }
        catch(ReflectiveOperationException e)
        {
            throw new AnimatedPropertyException(e);
        }
    }
}
