package edu.curtin.cs.codedemo.scenario;

import java.lang.annotation.*;

/**
 * <p>Used to provide key details about a scenario.</p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ScenarioDetails
{
    String name();
    String author();
    String iconFile() default "";
    String helpFile() default "";
}
