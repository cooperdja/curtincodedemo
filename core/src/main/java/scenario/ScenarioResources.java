package edu.curtin.cs.codedemo.scenario;

import java.io.*;
import java.awt.Image;
import java.util.*;
import java.util.jar.JarFile;
import java.util.logging.*;
import javax.imageio.ImageIO;

/**
 * <p>Used to acquire files (resources) from within a scenario's .jar file. At present, this 
 * includes images and text files.</p>
 */
public class ScenarioResources
{
    private static final Logger logger = Logger.getLogger(ScenarioResources.class.getName());

    private JarFile jarFile;
    private static Map<String,Image> imageCache = new HashMap<>();

    public ScenarioResources(JarFile jarFile)
    {
        this.jarFile = jarFile;
    }
    
    public InputStream getInputStream(String filename) throws IOException
    {
        return jarFile.getInputStream(jarFile.getEntry(filename));
    }
    
    /** 
     * <p>Reads an image file.</p>
     */
    public Image getImage(String filename) throws IOException
    {
        try
        {
            Image image = imageCache.get(filename);
            if(image == null)
            {
                image = ImageIO.read(getInputStream(filename));
                imageCache.put(filename, image);
            }
            return image;
        }
        catch(IOException e)
        {
            logger.log(
                Level.WARNING, 
                String.format(
                    "Failed to read image file '%s' from jar file '%s': %s", 
                    filename, jarFile, e.getMessage()),
                e
            );
            throw e;
        }
    }
    
    /**
     * <p>Reads a text file.</p>
     */
    public String getTextFile(String filename) throws IOException
    {
        // Adapted from https://stackoverflow.com/a/5445161
        Scanner sc = new Scanner(getInputStream(filename)).useDelimiter("\\A");
        String text = sc.next();
        
        IOException e = sc.ioException();
        if(e != null)
        {
            logger.log(
                Level.WARNING, 
                String.format("Failed to read text file '%s' from jar file '%s': %s", 
                    filename, jarFile, e.getMessage()),
                e
            );
            throw e;
        }
        
        return text;
    }
}
