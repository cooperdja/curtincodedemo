package edu.curtin.cs.codedemo.scenario.input;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface DoubleInput
{    
    double value();
    double min() default Double.MIN_VALUE;
    double max() default Double.MAX_VALUE;
    double step() default 1.0;
    
    int order() default Integer.MAX_VALUE;
    String label() default "";
}
