package edu.curtin.cs.codedemo.scenario.input;
import edu.curtin.cs.codedemo.scenario.Scenario;

import java.lang.reflect.Method;

public class DoubleInputHandler extends InputHandler<Double>
{   
    private DoubleInput annot;
    
    public DoubleInputHandler(Scenario scenario, Method handler, DoubleInput annot)
    {
        super(scenario, handler, annot);
        this.annot = annot;
    }
    
    public double getDefaultValue() { return annot.value(); }
    public double getMin() { return annot.min(); }
    public double getMax() { return annot.max(); }
    public double getStep() { return annot.step(); }
}
