package edu.curtin.cs.codedemo.scenario.input;
import edu.curtin.cs.codedemo.scenario.Scenario;

import java.lang.reflect.Method;

public class IntInputHandler extends InputHandler<Integer>
{   
    private IntInput annot;
    
    public IntInputHandler(Scenario scenario, Method handler, IntInput annot)
    {
        super(scenario, handler, annot);
        this.annot = annot;
    }
    
    public int getDefaultValue() { return annot.value(); }
    public int getMin() { return annot.min(); }
    public int getMax() { return annot.max(); }
    public int getStep() { return annot.step(); }
}
