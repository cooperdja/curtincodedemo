package edu.curtin.cs.codedemo.scenario.input;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ListInput
{    
    String[] value();
    String[] data() default {};
    
    int order() default Integer.MAX_VALUE;
    String label() default "";
}
