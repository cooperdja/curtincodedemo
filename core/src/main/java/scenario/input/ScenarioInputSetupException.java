package edu.curtin.cs.codedemo.scenario.input;
import edu.curtin.cs.codedemo.scenario.ScenarioSetupException;

public class ScenarioInputSetupException extends ScenarioSetupException
{
    public ScenarioInputSetupException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
    
    public ScenarioInputSetupException(String msg)
    {
        super(msg);
    }
}
