package edu.curtin.cs.codedemo.scenario.input;
import edu.curtin.cs.codedemo.scenario.Scenario;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

public class InputHandler<T>
{
    private Scenario scenario;
    private Method handler;
    
    private String label;
    private int order;
    
    public InputHandler(Scenario scenario, Method handler, Annotation annot)
    {
        this.scenario = scenario;
        this.handler = handler;
        
        Class<? extends Annotation> annotClass = annot.getClass();
        try
        {
            this.label = (String)annotClass.getMethod("label").invoke(annot);
            if("".equals(label))
            {
                this.label = null;
            }
            this.order = ((Integer)annotClass.getMethod("order").invoke(annot)).intValue();
        }
        catch(ReflectiveOperationException e)
        {
            throw new AssertionError("Internal error: " + e.getMessage());
        }
    }
    
    public String getLabel()
    {
        return label;
    }
    
    public int getOrder()
    {
        return order;
    }
    
    public void input(T data) throws ScenarioInputSetupException
    {
        try
        {
            handler.invoke(scenario, data);
        }
        catch(ReflectiveOperationException e)
        {
            String msg = String.format(
                "Unable to invoke method '%s.%s(): %s",
                scenario.getClass().getName(), handler.getName(), e.getMessage());
            throw new ScenarioInputSetupException(msg, e);
        }
    }
}
