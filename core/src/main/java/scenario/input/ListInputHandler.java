package edu.curtin.cs.codedemo.scenario.input;
import edu.curtin.cs.codedemo.scenario.Scenario;

import java.lang.reflect.Method;
import java.util.*;

public class ListInputHandler extends InputHandler<String>
{   
    private Map<String,String> inputMap;
    
    public ListInputHandler(Scenario scenario, Method handler, ListInput annot) 
        throws ScenarioInputSetupException
    {
        super(scenario, handler, annot);
        String[] labels = annot.value();
        String[] data = annot.data();
        
        if(data.length == 0)
        {
            data = labels;
        }
        else if(data.length != labels.length)
        {
            String msg = String.format(
                "For @%s method '%s()', 'value' and 'data' must have equal lengths.",
                annot.getClass().getName(),
                handler.toString());
            throw new ScenarioInputSetupException(msg);
        }
                            
        inputMap = new LinkedHashMap<>();
        for(int i = 0; i < labels.length; i++)
        {
            inputMap.put(labels[i], data[i]);
        }
    }
    
    public Map<String,String> getInputMap()
    {
        return Collections.unmodifiableMap(inputMap);
    }
}
