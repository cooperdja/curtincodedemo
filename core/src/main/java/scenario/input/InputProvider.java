package edu.curtin.cs.codedemo.scenario.input;
import edu.curtin.cs.codedemo.scenario.Scenario;
import edu.curtin.cs.codedemo.controller.LoadObserver;

import java.lang.reflect.Method;
import java.util.*;

public abstract class InputProvider implements LoadObserver
{   
    public InputProvider() {}
    
    @Override
    public void scenarioLoaded(Scenario scenario)
    {
        Class<? extends Scenario> cls = scenario.getClass();
        
        try
        {
            for(Method method : cls.getMethods())
            {
                for(InputInitialiser initialiser : getInputInitialisers())
                {
                    if(initialiser.tryFor(scenario, method))
                    {
                        break;
                    }
                }
            }
            
            finaliseSetup();
        }
        catch(ScenarioInputSetupException e)
        {
            setupFailed(e);
        }
    }
    
    protected abstract InputInitialiser[] getInputInitialisers();
    protected abstract void finaliseSetup();
    protected abstract void setupFailed(ScenarioInputSetupException e);
}
