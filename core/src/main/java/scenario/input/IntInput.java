package edu.curtin.cs.codedemo.scenario.input;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface IntInput
{    
    int value();
    int min() default Integer.MIN_VALUE;
    int max() default Integer.MAX_VALUE;
    int step() default 1;
    
    int order() default Integer.MAX_VALUE;
    String label() default "";
}
