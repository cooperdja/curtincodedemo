package edu.curtin.cs.codedemo.scenario.input;
import edu.curtin.cs.codedemo.scenario.Scenario;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public abstract class InputInitialiser<A extends Annotation, 
                                       H extends InputHandler,
                                       D>
{
    private Class<A> annotationType;
    private Class<H> handlerType;
    private Class<D> dataType;
    
    private InputInitialiser(Class<A> annotationType, Class<H> handlerType, Class<D> dataType)
    {
        this.annotationType = annotationType;
        this.handlerType = handlerType;
        this.dataType = dataType;
    }

    public boolean tryFor(Scenario scenario, Method method) throws ScenarioInputSetupException
    {
        A annot = method.getAnnotation(annotationType);
        if(annot == null)
        {
            return false;
        }
        
        Class<?>[] actualParameterTypes = method.getParameterTypes();
        if(actualParameterTypes.length != 1 || actualParameterTypes[0] != dataType)
        {
            String msg = String.format(
                "@%s method '%s' must take one parameter of type %s",
                annotationType.getName(), 
                method.toString(), 
                dataType.toString());
            throw new ScenarioInputSetupException(msg);
        }
    
        try
        {
            H handler = handlerType
                .getConstructor(Scenario.class, Method.class, annotationType)
                .newInstance(scenario, method, annot);
        
            connectHandler(handler);
            return true;
        }
        catch(InvocationTargetException e)
        {
            String msg = String.format(
                "For @%s method '%s': %s",
                annotationType.getName(),
                method.toString(),
                e.getMessage());
            throw new ScenarioInputSetupException(msg, e.getCause());
        }
        catch(ReflectiveOperationException e)
        {
            String msg = String.format(
                "Internal error in respect of @%s method '%s': %s",
                annotationType.getName(),
                method.toString(),
                e.getMessage());
            throw new ScenarioInputSetupException(msg, e);
        }
    }
    
    protected abstract void connectHandler(H handler);
    
    public abstract static class List 
        extends InputInitialiser<ListInput, ListInputHandler, String>
    {
        public List()
        {
            super(ListInput.class, ListInputHandler.class, String.class);
        }
    }

    public abstract static class Int 
        extends InputInitialiser<IntInput, IntInputHandler, Integer>
    {
        public Int()
        {
            super(IntInput.class, IntInputHandler.class, int.class);
        }
    }

    public abstract static class Double 
        extends InputInitialiser<DoubleInput, DoubleInputHandler, java.lang.Double>
    {
        public Double()
        {
            super(DoubleInput.class, DoubleInputHandler.class, double.class);
        }
    }
}
